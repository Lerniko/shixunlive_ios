//
//  SXRootTool.h
//  shixunLive
//
//  Created by 徐荣 on 2016/10/24.
//  Copyright © 2016年 xsili. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

@protocol rootToolDelegate <NSObject>

- (void)alreadlyGetUser;

@end

@interface SXRootTool : NSObject

/**
 *  选择根控制器
 */
+(void)chooseRootController:(UIWindow *)window;

/**
 *  选择登录控制器
 */
+(void)chooseLoginController:(UIWindow *)window;

@property (nonatomic, weak) id<rootToolDelegate> delegate;

@end
