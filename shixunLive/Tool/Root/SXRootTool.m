//
//  SXRootTool.m
//  shixunLive
//
//  Created by 徐荣 on 2016/10/24.
//  Copyright © 2016年 xsili. All rights reserved.
//

#import "SXRootTool.h"
#import "NSXLoginViewController.h"
#import "NSXTabBarController.h"

@implementation SXRootTool

+ (void)chooseRootController:(UIWindow *)window
{
    //    //获取当前版本号
    //    NSString *currentVersion = [NSBundle mainBundle].infoDictionary[@"CFBundleVersion"];
    //    //获取上一次的版本号
    //    NSString *lastVersion = [[NSUserDefaults standardUserDefaults]objectForKey:MYVersionKey];
    //    if ([currentVersion isEqualToString:lastVersion]) {
    //        //版本号相同，没有更新
    //        BETabBarController *tabbarVC = [BETabBarController new];
    //        window.rootViewController = tabbarVC;
    //    }else{
    //        //如果有新特性，进入新特性界面
    //        BENewFeatureController *newVC = [BENewFeatureController new];
    //        window.rootViewController = newVC;
    //        //保存当前的版本，用偏好设置
    //        [[NSUserDefaults standardUserDefaults]setObject:currentVersion forKey:MYVersionKey];
    //    }
    
    //跳转到home
    NSXTabBarController *tabBarVc = [NSXTabBarController new];
    window.rootViewController = tabBarVc;

    
    [SXAccount sharedAccount].login = NO;
    NSString *urlStr = [NSString stringWithFormat:@"%@/login/portal/login?",SXAPIURL];
    NSDictionary *dict = @{@"loginName":[SXAccount sharedAccount].loginUser,@"password":[SXAccount sharedAccount].loginPassword};
    AFHTTPSessionManager *manager = [AFHTTPSessionManager manager];
    [manager POST:urlStr parameters:dict progress:nil success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
        NSLog(@"%@ %@",task,responseObject);
        NSDictionary *responseDict = responseObject;
        NSString *msg = [responseDict objectForKey:@"msg"];
        NSString *errorCode = [responseDict objectForKey:@"errorCode"];
        NSDictionary *data = [responseDict objectForKey:@"data"];
        NSNumber *userType = [data objectForKey:@"userType"];
        NSHTTPURLResponse *httpURLResponse = (NSHTTPURLResponse *)task.response;
        NSDictionary *allHeaders = httpURLResponse.allHeaderFields;
        NSString *jsessionID = [allHeaders objectForKey:@"Set-Cookie"];
        NSArray *array = [jsessionID componentsSeparatedByString:@";"];
        jsessionID = array[0];
        NSLog(@"%@====",jsessionID);
        NSLog(@"%@ %@ %@",data,errorCode,msg);
        if ([userType isEqualToNumber:@2]) {
            //把这次登录获取的jsessionID存放在account单例
            [SXAccount sharedAccount].jsessionID = jsessionID;
            [SXAccount sharedAccount].login = YES;
            //保存到沙盒
            [[SXAccount sharedAccount] saveToSandBox];
            
            NSString *getUserUrl = [NSString stringWithFormat:@"%@/portal/mycenter/getLoginUser",SXAPIURL];
            getUserUrl = [NSString stringWithFormat:@"%@?%@",getUserUrl,[SXAccount sharedAccount].jsessionID];
            
            NSLog(@"edggd %@",getUserUrl);
            
            [ServiceAPIManager ServiceAPIManagerForGET:getUserUrl parameters:nil success:^(id  _Nonnull json) {
                NSDictionary *dict = json;
                NSLog(@"%@",dict);
                NSDictionary *data = [dict objectForKey:@"data"];
                NSLog(@"%@",data);
                
                NSNumber *errorCode = dict[@"errorCode"];
                if ([errorCode isEqualToNumber:@0]) {
                    NSString *userName = [NSString new];
                    if ((NSString *)data[@"userName"] != nil &&  !((data[@"userName"] == [NSNull null]))) {
                        userName = [data objectForKey:@"userName"];
                    }
                    NSString *headerImg = [NSString new];
                    if ((NSString *)data[@"headerImg"] != nil && !(data[@"headerImg"] == [NSNull null])) {
                        headerImg = [data objectForKey:@"headerImg"];
                    }
                    NSString *userID = [NSString new];
                    if ((NSString *)data[@"id"] != nil && !(data[@"id"] == [NSNull null])) {
                        userID = [data objectForKey:@"id"];
                    }
                    [SXAccount sharedAccount].userName = userName;
                    [SXAccount sharedAccount].headerImg = headerImg;
                    [SXAccount sharedAccount].userID = userID;
                    
                    NSNotification *noti = [NSNotification notificationWithName:@"account" object:[SXAccount sharedAccount]];
                    [LLFNotiCenter postNotification:noti];
                }
                
            } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
                NSString *errorStr = [error.userInfo objectForKey:@"NSLocalizedDescription"];
                if ([errorStr isEqualToString:@""] || errorStr == NULL) {
                    NSLog(@"errorcode 为空");
                }else{
                    [SVProgressHUD showErrorWithStatus:errorStr];
                    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(1.5 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
                        [SVProgressHUD dismiss];
                    });
                }
            }];
        }

        
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        NSLog(@"%@",error);
        [SVProgressHUD showErrorWithStatus:@"登录失败，请重新登录"];
        dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(1.5 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
            [SVProgressHUD dismiss];
        });
        [self chooseLoginController:window];
    }];
}

+(void)chooseLoginController:(UIWindow *)window
{
    NSXLoginViewController *loginVC = [NSXLoginViewController new];
    UINavigationController *nav = [[UINavigationController alloc]initWithRootViewController:loginVC];
    window.rootViewController = nav;
}


@end
