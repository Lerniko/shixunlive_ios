//
//  CALayer+NSXCALayer.m
//  shixunLive
//
//  Created by apple on 2018/1/18.
//  Copyright © 2018年 xsili. All rights reserved.
//

#import "CALayer+NSXCALayer.h"

@implementation CALayer (NSXCALayer)


- (UIColor *)borderColorFromUIColor {
    
    return objc_getAssociatedObject(self, @selector(borderColorFromUIColor));
}
-(void)setBorderColorFromUIColor:(UIColor *)color
{
    objc_setAssociatedObject(self, @selector(borderColorFromUIColor), color, OBJC_ASSOCIATION_RETAIN_NONATOMIC);
    [self setBorderColorFromUI:color];
}
- (void)setBorderColorFromUI:(UIColor *)color
{
    self.borderColor = color.CGColor;
    
}

@end
