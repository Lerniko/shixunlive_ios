//
//  CALayer+NSXCALayer.h
//  shixunLive
//
//  Created by apple on 2018/1/18.
//  Copyright © 2018年 xsili. All rights reserved.
//

#import <QuartzCore/QuartzCore.h>

@interface CALayer (NSXCALayer)//边框颜色设置扩展

@property(nonatomic, strong) UIColor *borderColorFromUIColor;

@end
