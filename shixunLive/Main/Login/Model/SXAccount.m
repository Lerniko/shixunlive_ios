//
//  SXAccount.m
//  shixunLive
//
//  Created by 徐荣 on 2016/12/1.
//  Copyright © 2016年 xsili. All rights reserved.
//

#import "SXAccount.h"

#define LoginType @"loginType"
#define userKey @"user"
#define passwordKey @"password"
#define UserType @"userType"
#define JSESSIONID @"JSESSIONID"
#define loginKey @"login"
#define loginTime @"loginTime"
#define FollowNum @"followNum"
#define FansNum @"fansNum"

@implementation SXAccount

+ (instancetype)sharedAccount
{
    return [[self alloc]init];
}

+ (instancetype)allocWithZone:(struct _NSZone *)zone
{
    static SXAccount *account;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        account = [super allocWithZone:zone];
        NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
        account.loginType = [defaults objectForKey:LoginType];
        account.loginUser = [defaults objectForKey:userKey];
        account.loginPassword = [defaults objectForKey:passwordKey];
        account.loginUserType = [defaults objectForKey:UserType];
        account.jsessionID = [defaults objectForKey:JSESSIONID];
        account.login = [defaults boolForKey:loginKey];
        account.loginDateString=[defaults objectForKey:loginTime];
        account.followNum = [defaults objectForKey:FollowNum];
        account.fansNum = [defaults objectForKey:FansNum];
    });
    return account;
}

- (void)saveToSandBox
{
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    [defaults setObject:_loginType forKey:LoginType];
    [defaults setObject:_loginUser forKey:userKey];
    [defaults setObject:_loginPassword forKey:passwordKey];
    [defaults setObject:_loginUserType forKey:UserType];
    [defaults setObject:_jsessionID forKey:JSESSIONID];
    [defaults setBool:self.isLogin forKey:loginKey];
    [defaults setObject:_loginDateString forKey:loginTime];
    [defaults setObject:_followNum forKey:FollowNum];
    [defaults setObject:_fansNum forKey:FansNum];
    [defaults synchronize];
}

- (void)removeFromSandBox
{
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    [defaults removeObjectForKey:userKey];
    [defaults removeObjectForKey:passwordKey];
    [defaults removeObjectForKey:UserType];
    [defaults removeObjectForKey:JSESSIONID];
    [defaults removeObjectForKey:loginKey];
    [defaults removeObjectForKey:loginTime];
    [defaults removeObjectForKey:FollowNum];
    [defaults removeObjectForKey:FansNum];
}

@end

