//
//  SXAccount.h
//  shixunLive
//
//  Created by 徐荣 on 2016/12/1.
//  Copyright © 2016年 xsili. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface SXAccount : NSObject


/**
 *  用户登录的类型 (用来记录上次登录的类型)
 */
@property (nonatomic, copy)NSString *loginType;
/**
 *  用户登录的登录名(账号)
 */
@property (nonatomic, copy)NSString *loginUser;
/**
 *  用户登录的密码
 */
@property (nonatomic, copy)NSString *loginPassword;
/**
 *  用户角色类型(2=老师,1=学生)
 */
@property (nonatomic, copy)NSString *loginUserType;
/**
 *  判断用户是否登录
 */
@property(nonatomic,assign,getter=isLogin) BOOL login;
/**
 *  判断用户是否Vip
 */
@property (nonatomic,assign)int isVip;

/**
 *  jsessionID
 */
@property (nonatomic, copy) NSString *jsessionID;

/*
 * userName
 */
@property (nonatomic, copy) NSString *userName;

/*
 * nickName
 */
@property (nonatomic, copy) NSString *nickName;

/*
 * nickName
 */
@property (nonatomic, assign) int sex;

/*
 * headerImg
 */
@property (nonatomic, copy) NSString *headerImg;

/*
 * userID
 */
@property (nonatomic, copy) NSString *userID;

/*
 * phone
 */
@property (nonatomic, copy) NSString *phone;

/*
 * followNum
 */
@property (nonatomic, copy) NSString *followNum;

/*
 * fansNum
 */
@property (nonatomic, copy) NSString *fansNum;

/*
 * date
 */
@property (nonatomic, copy) NSString *loginDateString;

/*
 * inviteCode
 */
@property (nonatomic, copy) NSString *inviteCode;
/**
 *  单例
 */
+(instancetype)sharedAccount;

/**
 *  保存最新的登录用户数据到沙盒里面去
 */
-(void)saveToSandBox;


- (void)removeFromSandBox;


@end

