//
//  NSXTabBarController.m
//  shixunLive
//
//  Created by apple on 2019/12/31.
//  Copyright © 2019 MSEducation. All rights reserved.
//

#import "NSXTabBarController.h"
#import "NSXHomeViewController.h"
#import "NSXLearnViewController.h"
#import "NSXDiscoverViewController.h"
#import "NSXMallViewController.h"
#import "NSXMineViewController.h"


@interface NSXTabBarController ()

@end

@implementation NSXTabBarController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.view.backgroundColor = LLFDefaultBgColor;
    [self setupAllChildViewControllers];
}

- (void)setupAllChildViewControllers{
    NSXHomeViewController *homeVC=[[NSXHomeViewController alloc] init];
    [self setTabBarItem:homeVC.tabBarItem
                  title:@"师讯"
              titleSize:11.0
          selectedImage:@"icon_home_selected"
     selectedTitleColor:[UIColor blackColor]
            normalImage:@"icon_home"
       normalTitleColor:[UIColor blackColor]];
    
    NSXLearnViewController *learnVC=[[NSXLearnViewController alloc] init];
    [self setTabBarItem:learnVC.tabBarItem
                  title:@"学习"
              titleSize:11.0
          selectedImage:@"icon_learn_selected"
     selectedTitleColor:[UIColor blackColor]
            normalImage:@"icon_learn"
       normalTitleColor:[UIColor blackColor]];
    
    NSXDiscoverViewController *discoverVC=[[NSXDiscoverViewController alloc] init];
    [self setTabBarItem:discoverVC.tabBarItem
                  title:@"探索"
              titleSize:11.0
          selectedImage:@"icon-discover_selected"
     selectedTitleColor:[UIColor blackColor]
            normalImage:@"icon-discover"
       normalTitleColor:[UIColor blackColor]];
    
    NSXMallViewController *mallVC=[[NSXMallViewController alloc] init];
    [self setTabBarItem:mallVC.tabBarItem
                  title:@"商城"
              titleSize:11.0
          selectedImage:@"icon_mall_selected"
     selectedTitleColor:[UIColor blackColor]
            normalImage:@"icon_mall"
       normalTitleColor:[UIColor blackColor]];
    
    NSXMineViewController *mineVC=[[NSXMineViewController alloc] init];
    [self setTabBarItem:mineVC.tabBarItem
                  title:@"我的"
              titleSize:11.0
          selectedImage:@"icon_mine_selected"
     selectedTitleColor:[UIColor blackColor]
            normalImage:@"icon_mine"
       normalTitleColor:[UIColor blackColor]];
    
    
    self.viewControllers = @[homeVC,learnVC,discoverVC,mallVC,mineVC];
}

- (void)setTabBarItem:(UITabBarItem *)tabbarItem
                title:(NSString *)title
            titleSize:(CGFloat)size
        selectedImage:(NSString *)selectedImage
   selectedTitleColor:(UIColor *)selectColor
          normalImage:(NSString *)unselectedImage
     normalTitleColor:(UIColor *)unselectColor
{
    //设置图片
    tabbarItem = [tabbarItem initWithTitle:title image:[[UIImage imageNamed:unselectedImage]imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal] selectedImage:[[UIImage imageNamed:selectedImage]imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal]];
    
    // S未选中字体颜色
    [[UITabBarItem appearance] setTitleTextAttributes:@{NSForegroundColorAttributeName:unselectColor,NSFontAttributeName:LLFFontPingFangSC(size)} forState:UIControlStateNormal];
    // 选中字体颜色
    [[UITabBarItem appearance] setTitleTextAttributes:@{NSForegroundColorAttributeName:selectColor,NSFontAttributeName:LLFFontPingFangSC(size)} forState:UIControlStateSelected];
}

@end
