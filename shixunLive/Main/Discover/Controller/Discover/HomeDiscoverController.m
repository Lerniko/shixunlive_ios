//
//  HomeDiscoverController.m
//  shixunLive
//
//  Created by apple on 2019/12/31.
//  Copyright © 2019 MSEducation. All rights reserved.
//

#import "HomeDiscoverController.h"

@interface HomeDiscoverController ()

@property (nonatomic,strong)UIScrollView * scrollView;

@end

@implementation HomeDiscoverController

- (void)viewDidLoad {
    [super viewDidLoad];
    
}

#pragma mark - 控件
-(UIScrollView *)preferScrollView
{
    return self.scrollView;
}

@end
