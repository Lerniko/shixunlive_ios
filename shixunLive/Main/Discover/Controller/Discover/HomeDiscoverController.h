//
//  HomeDiscoverController.h
//  shixunLive
//
//  Created by apple on 2019/12/31.
//  Copyright © 2019 MSEducation. All rights reserved.
//

#import "SPPageProtocol.h"

NS_ASSUME_NONNULL_BEGIN

@interface HomeDiscoverController : UIViewController<SPPageSubControllerDataSource>

@property (nonatomic,copy) void (^HomeDiscoverControllerBlock)(BOOL BlackViewBeClick);

@end

NS_ASSUME_NONNULL_END
