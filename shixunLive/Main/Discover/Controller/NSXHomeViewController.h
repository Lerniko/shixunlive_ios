//
//  NSXHomeViewController.h
//  shixunLive
//
//  Created by apple on 2019/12/31.
//  Copyright © 2019 MSEducation. All rights reserved.
//

#import "SPCoverController.h"

NS_ASSUME_NONNULL_BEGIN

@protocol NSXHomeVCDelegate<NSObject>

-(void)categoryBtnClickWithParameters:(id)parameters;

@end

@interface NSXHomeViewController : SPCoverController

@property (nonatomic,weak)id<NSXHomeVCDelegate> delegate;

@end

NS_ASSUME_NONNULL_END
