//
//  StudyDatumController.m
//  shixunLive
//
//  Created by apple on 2020/1/3.
//  Copyright © 2020 MSEducation. All rights reserved.
//

#import "StudyDatumController.h"
#import "StudyDatumBannerView.h"
#import "PreschoolKnowledgeCategoryView.h"
#import "StudyDatumListCell.h"

#define CategoryViewW 88
#define CellID @"StudyDatumListCell"
@interface StudyDatumController ()<UITableViewDelegate,UITableViewDataSource>

@property (nonatomic,strong)UIScrollView * scrollView;

@property(nonatomic,strong)StudyDatumBannerView * bannerView;

@property(nonatomic,strong)PreschoolKnowledgeCategoryView * categoryView;

@property(nonatomic,strong)UITableView * tableView;

@property(nonatomic,strong)NSMutableArray * listArray;

@end

@implementation StudyDatumController
#pragma mark - 数据
-(NSMutableArray *)listArray{
    if(!_listArray){
        _listArray=[NSMutableArray new];
    }
    return _listArray;
}

#pragma mark - UI
-(PreschoolKnowledgeCategoryView *)categoryView{
    if(!_categoryView){
        _categoryView=[[PreschoolKnowledgeCategoryView alloc]init];
        _categoryView.showsVerticalScrollIndicator=NO;
    }
    return _categoryView;
}

-(UITableView *)tableView{
    if(!_tableView){
        _tableView = [[UITableView alloc]initWithFrame:CGRectMake(0, 0, MYScreenWidth, MYScreenHeight) style:UITableViewStylePlain];
        _tableView.delegate=self;
        _tableView.dataSource=self;
        _tableView.tableFooterView = [[UIView alloc]initWithFrame:CGRectZero];
    }
    return _tableView;
}

-(StudyDatumBannerView *)bannerView{
    if(!_bannerView){
        _bannerView=[[StudyDatumBannerView alloc]init];
    }
    return _bannerView;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    [self layoutUI];
    [self uploadContent];
}

#pragma mark - 控件
-(UIScrollView *)preferScrollView
{
    return self.scrollView;
}

#pragma mark - 布局
-(void)layoutUI{
    [self.view addSubview:self.categoryView];
    [self.view addSubview:self.tableView];
    
    [self.categoryView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self.view);
        make.top.equalTo(self.view).offset(SafeAreaTopHeight+12);
        make.width.mas_equalTo(CategoryViewW);
        if(KisiOS11){
            make.bottom.equalTo(self.view.mas_safeAreaLayoutGuideBottom).offset(-TabBarHeight);
        }else{
            make.bottom.equalTo(self.view).offset(-TabBarHeight);
        }
    }];
    
    [self.tableView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.view).offset(SafeAreaTopHeight);
        make.left.equalTo(self.categoryView.mas_right);
        make.right.equalTo(self.view);
        if(KisiOS11){
            make.bottom.equalTo(self.view.mas_safeAreaLayoutGuideBottom).offset(-TabBarHeight);
        }else{
            make.bottom.equalTo(self.view).offset(-TabBarHeight);
        }
    }];
}

-(void)uploadContent{
    self.categoryView.listArray = @[@"热门分类",@"热门课程",@"上新课程",@"教学技能",@"幼儿教学",@"个人提升"];
}

#pragma mark - UITableViewDelegate
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return 10;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    StudyDatumListCell* cell = nil;
    cell = [tableView dequeueReusableCellWithIdentifier:CellID];
    if (cell == nil) {
        cell = [[NSBundle mainBundle] loadNibNamed:CellID owner:nil options:nil].firstObject;
        cell.selectedBackgroundView = [[UIView alloc] initWithFrame:cell.frame];
        cell.selectedBackgroundView.backgroundColor = [UIColor clearColor];
        //        cell.model = self.categoryArray[indexPath.row];
    }
    return cell;
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    //    ExpressSingleInfoModel * model=self.listArray[indexPath.row];
    //    CGFloat cellHeight=[model.cellHeight floatValue];
    //    if (cellHeight == 0.f) {    // 如果字典中没有存储cell的高度
    //        cellHeight += 50;
    //        NSString * text = model.status;
    //        NSMutableDictionary * dic = [NSMutableDictionary dictionary];
    //        dic[NSFontAttributeName] = [UIFont systemFontOfSize:13];
    //        CGRect textRect = [text boundingRectWithSize:CGSizeMake((MYScreenWidth-50), MAXFLOAT)  options:NSStringDrawingUsesLineFragmentOrigin attributes:dic context:nil];
    //        cellHeight += textRect.size.height;
    //        model.cellHeight = [NSString stringWithFormat:@"%d",(int)cellHeight];
    //    }
    //    return cellHeight;
    return 89;
}

-(UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section{
    return self.bannerView;
}

@end
