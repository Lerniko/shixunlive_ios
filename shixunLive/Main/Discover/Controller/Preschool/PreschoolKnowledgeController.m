//
//  PreschoolKnowledgeController.m
//  shixunLive
//
//  Created by apple on 2020/1/3.
//  Copyright © 2020 MSEducation. All rights reserved.
//

#import "PreschoolKnowledgeController.h"
#import "PreschoolKnowledgeCategoryView.h"
#import "PreschoolKnowledgeListCell.h"

#define CellID @"PreschoolKnowledgeListCell"

#define CategoryViewW 88
@interface PreschoolKnowledgeController ()<UICollectionViewDelegate,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout>

@property(nonatomic,strong)UIScrollView * scrollView;

@property(nonatomic,strong)PreschoolKnowledgeCategoryView * categoryView;

@property(nonatomic,strong)UICollectionView * collectionView;

@property(nonatomic,strong)NSMutableArray * listArray;

@end

@implementation PreschoolKnowledgeController
#pragma mark - 数据
-(NSMutableArray *)listArray{
    if(!_listArray){
        _listArray=[NSMutableArray new];
    }
    return _listArray;
}

#pragma mark - UI
-(PreschoolKnowledgeCategoryView *)categoryView{
    if(!_categoryView){
        _categoryView=[[PreschoolKnowledgeCategoryView alloc]init];
        _categoryView.showsVerticalScrollIndicator=NO;
    }
    return _categoryView;
}

-(UICollectionView *)collectionView{
    if(!_collectionView){
        UICollectionViewFlowLayout *flowLayout = [[UICollectionViewFlowLayout alloc] init];
        flowLayout.itemSize = CGSizeMake(MYScreenWidth-CategoryViewW-24,116);
        _collectionView = [[UICollectionView alloc]initWithFrame:self.view.bounds collectionViewLayout:flowLayout];
        _collectionView.dataSource = self;
        _collectionView.delegate = self;
        _collectionView.backgroundColor=[UIColor whiteColor];
        _collectionView.showsVerticalScrollIndicator=NO;
        _collectionView.mj_header = [MJRefreshNormalHeader headerWithRefreshingTarget:self refreshingAction:@selector(uploadContent)];
        _collectionView.mj_footer = [MJRefreshBackNormalFooter footerWithRefreshingTarget:self refreshingAction:@selector(loadContent)];
        [_collectionView registerNib:[UINib nibWithNibName:CellID bundle:nil] forCellWithReuseIdentifier:CellID];
    }
    return _collectionView;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [self layoutUI];
    [self uploadContent];
}

#pragma mark - 布局
-(void)layoutUI{
    [self.view addSubview:self.categoryView];
    [self.view addSubview:self.collectionView];
    [self.categoryView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self.view);
        make.top.equalTo(self.view).offset(SafeAreaTopHeight+12);
        make.width.mas_equalTo(CategoryViewW);
        if(KisiOS11){
            make.bottom.equalTo(self.view.mas_safeAreaLayoutGuideBottom).offset(-TabBarHeight);
        }else{
            make.bottom.equalTo(self.view).offset(-TabBarHeight);
        }
    }];
    
    [self.collectionView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self.categoryView.mas_right);
        make.right.equalTo(self.view);
        make.top.equalTo(self.view).offset(SafeAreaTopHeight);
        if(KisiOS11){
            make.bottom.equalTo(self.view.mas_safeAreaLayoutGuideBottom);
        }else{
            make.bottom.equalTo(self.view);
        }
    }];
}

#pragma mark - 控件
-(UIScrollView *)preferScrollView
{
    return self.scrollView;
}

#pragma mark - 接口调用
-(void)uploadContent{
    self.categoryView.listArray = @[@"热门分类",@"热门课程",@"上新课程",@"教学技能",@"幼儿教学",@"个人提升"];
}

-(void)loadContent{
    
}

#pragma mark - UICollectionViewDataSource
- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section{
    return 10;
}

- ( UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath{
    PreschoolKnowledgeListCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:CellID forIndexPath:indexPath];
    cell.selectedBackgroundView = [[UIView alloc] initWithFrame:cell.frame];
    cell.selectedBackgroundView.backgroundColor = [UIColor clearColor];
//    cell.model = model;
    [self setCellStyle:cell];
    cell.backgroundColor = [UIColor whiteColor];
    return cell;
}

-(void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath{
    
}

#pragma mark - UICollectionViewDelegateFlowLayout
-(UIEdgeInsets)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout insetForSectionAtIndex:(NSInteger)section
{
    return UIEdgeInsetsMake(12,12,12,12);
}

#pragma mark - Other
-(void)setCellStyle:(PreschoolKnowledgeListCell *)cell{
    cell.contentView.layer.backgroundColor = [UIColor whiteColor].CGColor;
    cell.contentView.layer.cornerRadius =8.0f;
    cell.contentView.layer.borderWidth =1.0f;
    cell.contentView.layer.borderColor = [UIColor clearColor].CGColor;
    cell.contentView.layer.masksToBounds =YES;
    cell.layer.shadowColor = LLFColor(238, 238, 238).CGColor;
    cell.layer.shadowOffset = CGSizeMake(0,2.0f);
    cell.layer.shadowRadius =10.0f;
    cell.layer.shadowOpacity =1.0f;
    cell.layer.masksToBounds =NO;
    cell.layer.cornerRadius=8.0f;
    cell.layer.shadowPath = [UIBezierPath bezierPathWithRoundedRect:cell.bounds cornerRadius:cell.contentView.layer.cornerRadius].CGPath;
}

@end
