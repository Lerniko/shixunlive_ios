//
//  ArticleDetailController.m
//  shixunLive
//
//  Created by apple on 2020/1/6.
//  Copyright © 2020 MSEducation. All rights reserved.
//

#import "ArticleDetailController.h"
#import "ArticleCommentDetailController.h"

#import "CommonAdView.h"
#import "ArticleDetailCommentCell.h"

#import "ArticleDetailHeaderView.h"
#import "ArticleDetailBottomView.h"
#import "ArticleDetailHeaderTitleView.h"
#import "ArticleCommentSectionHeaderView.h"
#import "ArticleNoCommentView.h"

#define CellID @"ArticleDetailCommentCell"
#define BottomViewH 56
@interface ArticleDetailController ()<UITableViewDelegate,UITableViewDataSource>

@property(nonatomic,strong)ArticleDetailHeaderTitleView * titleView;

@property(nonatomic,strong)ArticleDetailHeaderView * headerView;

@property(nonatomic,strong)ArticleDetailBottomView * bottomView;

@property(nonatomic,strong)CommonAdView * adView;

@property(nonatomic,strong)ArticleCommentSectionHeaderView * commentHeaderView;

@property(nonatomic,strong)ArticleNoCommentView * noCommentView;
///主视图
@property(nonatomic,strong)UIScrollView * scrollView;

@property(nonatomic,strong)NSMutableArray * listArray;

@property(nonatomic,strong)UITableView * tableView;

@end

@implementation ArticleDetailController
#pragma mark - 数据
-(NSMutableArray *)listArray{
    if(!_listArray){
        _listArray = [NSMutableArray new];
    }
    return _listArray;
}

#pragma markm - UI
-(UIScrollView *)scrollView{
    if(!_scrollView){
        _scrollView=[[UIScrollView alloc]init];
        _scrollView.showsVerticalScrollIndicator=NO;
    }
    return _scrollView;
}

-(ArticleDetailHeaderTitleView *)titleView{
    if(!_titleView){
        _titleView=[[ArticleDetailHeaderTitleView alloc]initWithFrame:CGRectMake(0, 0, MYScreenWidth, 40)];
        _titleView.backgroundColor = [UIColor whiteColor];
    }
    return _titleView;
}

-(ArticleDetailHeaderView *)headerView{
    if(!_headerView){
        _headerView=[[NSBundle mainBundle]loadNibNamed:@"ArticleDetailHeaderView" owner:self options:nil].firstObject;
    }
    return _headerView;
}

-(ArticleDetailBottomView *)bottomView{
    if(!_bottomView){
        _bottomView=[[NSBundle mainBundle]loadNibNamed:@"ArticleDetailBottomView" owner:self options:nil].firstObject;
        [_bottomView.commentBtn addTarget:self action:@selector(commentBtnClick) forControlEvents:UIControlEventTouchUpInside];
    }
    return _bottomView;
}

-(CommonAdView *)adView{
    if(!_adView){
        _adView=[[NSBundle mainBundle]loadNibNamed:@"CommonAdView" owner:self options:nil].firstObject;
    }
    return _adView;
}

-(ArticleCommentSectionHeaderView *)commentHeaderView{
    if(!_commentHeaderView){
        _commentHeaderView=[[ArticleCommentSectionHeaderView alloc]init];
    }
    return _commentHeaderView;
}

-(UITableView *)tableView{
    if(!_tableView){
        _tableView = [[UITableView alloc]initWithFrame:CGRectMake(0, 0, MYScreenWidth, MYScreenHeight) style:UITableViewStylePlain];
        _tableView.delegate=self;
        _tableView.dataSource=self;
        _tableView.scrollEnabled=NO;
        _tableView.backgroundColor = [UIColor whiteColor];
        _tableView.tableFooterView = [[UIView alloc]initWithFrame:CGRectZero];
        [_tableView registerClass:[ArticleDetailCommentCell class] forCellReuseIdentifier:CellID];
    }
    return _tableView;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.view.backgroundColor = LLFColor(238,238,238);
    [self setNavBar];
    [self layoutUI];
}

#pragma mark - 布局
-(void)setNavBar{
    self.navigationController.navigationBarHidden=NO;
    UIButton *leftBtn = [[UIButton alloc]initWithFrame:CGRectMake(0, 0, 30, 30)];
    [leftBtn setImage:[UIImage imageNamed:@"icon_comeBack"] forState:0];
    leftBtn.titleLabel.font = [UIFont systemFontOfSize:10];
    [leftBtn addTarget:self action:@selector(comeBack) forControlEvents:UIControlEventTouchUpInside];
    UIBarButtonItem *leftItem = [[UIBarButtonItem alloc]initWithCustomView:leftBtn];
    self.navigationItem.leftBarButtonItem = leftItem;
    
    UIButton *rightBtn = [[UIButton alloc]initWithFrame:CGRectMake(0, 0, 30, 30)];
    [rightBtn setImage:[UIImage imageNamed:@"icon_article_share"] forState:0];
    rightBtn.titleLabel.font = [UIFont systemFontOfSize:10];
    [rightBtn addTarget:self action:@selector(share) forControlEvents:UIControlEventTouchUpInside];
    UIBarButtonItem *rightItem = [[UIBarButtonItem alloc]initWithCustomView:rightBtn];
    self.navigationItem.rightBarButtonItem = rightItem;
}

-(void)layoutUI{
    [self.view addSubview:self.scrollView];
    [self.scrollView addSubview:self.titleView];
    [self.scrollView addSubview:self.headerView];
    [self.scrollView addSubview:self.adView];
//    [self.scrollView addSubview:self.noCommentView];
    [self.scrollView addSubview:self.tableView];
    [self.view addSubview:self.bottomView];
    [self.scrollView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.view).offset(SafeAreaTopHeight);
        make.left.equalTo(self.view);
        make.width.mas_equalTo(MYScreenWidth);
        if(KisiOS11){
            make.bottom.equalTo(self.view.mas_safeAreaLayoutGuideBottom).offset(-BottomViewH);
        }else{
            make.bottom.equalTo(self.view).offset(-BottomViewH);
        }
    }];
    
    [self.titleView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.scrollView);
        make.width.mas_equalTo(MYScreenWidth);
        make.left.equalTo(self.scrollView);
    }];
    
    [self.headerView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.titleView.mas_bottom);
        make.left.equalTo(self.scrollView);
        make.width.mas_equalTo(MYScreenWidth);
        make.height.mas_equalTo(400);
    }];
    
    [self.adView mas_makeConstraints:^(MASConstraintMaker *make) {
        
        make.width.mas_equalTo(MYScreenWidth);
        make.left.equalTo(self.scrollView);
        make.bottom.equalTo(self.scrollView).offset(-8);
    }];
    
    [self.tableView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.headerView.mas_bottom).offset(8);
        make.left.equalTo(self.scrollView);
        make.width.mas_equalTo(MYScreenWidth);
        make.bottom.equalTo(self.adView.mas_top).offset(-8);
        make.height.mas_equalTo(270);
    }];
    
    [self.bottomView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.right.equalTo(self.view);
        if(KisiOS11){
            make.bottom.equalTo(self.view.mas_safeAreaLayoutGuideBottom);
        }else{
            make.bottom.equalTo(self.view);
        }
    }];
}

#pragma mark - UITableViewDelegate
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return 2;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    ArticleDetailCommentCell *cell = [tableView dequeueReusableCellWithIdentifier:CellID];
//    cell.model =self.listArray[indexPath.row];
    cell.selectedBackgroundView = [[UIView alloc] initWithFrame:cell.frame];
    cell.selectedBackgroundView.backgroundColor = [UIColor clearColor];
    return cell;
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
//    ExpressSingleInfoModel * model=self.listArray[indexPath.row];
//    CGFloat cellHeight=[model.cellHeight floatValue];
//    if (cellHeight == 0.f) {    // 如果字典中没有存储cell的高度
//        cellHeight += 50;
//        NSString * text = model.status;
//        NSMutableDictionary * dic = [NSMutableDictionary dictionary];
//        dic[NSFontAttributeName] = [UIFont systemFontOfSize:13];
//        CGRect textRect = [text boundingRectWithSize:CGSizeMake((MYScreenWidth-50), MAXFLOAT)  options:NSStringDrawingUsesLineFragmentOrigin attributes:dic context:nil];
//        cellHeight += textRect.size.height;
//        model.cellHeight = [NSString stringWithFormat:@"%d",(int)cellHeight];
//    }
//    return cellHeight;
    return 123;
}

-(UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section{
    return self.commentHeaderView;
}

#pragma mark - 按钮事件
-(void)comeBack{
    [self.navigationController popViewControllerAnimated:YES];
}

-(void)commentBtnClick{
    ArticleCommentDetailController * vc = [ArticleCommentDetailController new];
    [self.navigationController pushViewController:vc animated:YES];
}

@end
