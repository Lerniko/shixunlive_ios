//
//  HomeAttentionController.m
//  shixunLive
//
//  Created by apple on 2019/12/31.
//  Copyright © 2019 MSEducation. All rights reserved.
//

#import "HomeAttentionController.h"
#import "ArticleDetailController.h"
#import "HomeCategoryView.h"

#import "HomeAttentionCell.h"
#import "HomeAdCell.h"

#import "ArticleListModel.h"

#define CategoryViewH 40

#define ADCellID @"HomeAdCell"
#define CellID @"HomeAttentionCell"

#define ListUrl @"/portal/article/list"
@interface HomeAttentionController ()<UIScrollViewDelegate,UICollectionViewDelegate,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout>
///主视图
@property (nonatomic,strong)UIScrollView * scrollView;
@property (nonatomic,strong)UICollectionView * collectionView;
///分类
@property (nonatomic,strong)UIScrollView * categoryScrollView;
@property (nonatomic,strong)HomeCategoryView * categoryView;
///数据
@property (nonatomic,strong)NSMutableArray *categoryArr;
@property (nonatomic,strong)NSMutableArray *listArray;

@property (nonatomic,strong)NSMutableDictionary * paramaters;

@end

@implementation HomeAttentionController
#pragma mark - 数据
-(NSMutableArray *)categoryArr{
    if(!_categoryArr){
        _categoryArr=[NSMutableArray new];
    }
    return _categoryArr;
}

-(NSMutableArray *)listArray{
    if(!_listArray){
        _listArray=[NSMutableArray new];
    }
    return _listArray;
}

#pragma mark - 参数
-(NSMutableDictionary *)paramaters{
    if(!_paramaters){
        _paramaters=[NSMutableDictionary new];
    }
    return _paramaters;
}

#pragma mark - UI
-(UIScrollView *)scrollView{
    if(!_scrollView){
        _scrollView = [[UIScrollView alloc]init];
    }
    return _scrollView;
}

-(UICollectionView *)collectionView{
    if(!_collectionView){
        UICollectionViewFlowLayout *flowLayout = [[UICollectionViewFlowLayout alloc] init];
        flowLayout.itemSize = CGSizeMake(MYScreenWidth,100);
        _collectionView = [[UICollectionView alloc]initWithFrame:self.view.bounds collectionViewLayout:flowLayout];
        _collectionView.dataSource = self;
        _collectionView.delegate = self;
        _collectionView.backgroundColor=LLFColor(238, 238, 238);
        _collectionView.showsVerticalScrollIndicator=NO;
        _collectionView.mj_header = [MJRefreshNormalHeader headerWithRefreshingTarget:self refreshingAction:@selector(uploadContent)];
        _collectionView.mj_footer = [MJRefreshBackNormalFooter footerWithRefreshingTarget:self refreshingAction:@selector(loadContent)];
        [_collectionView registerClass:[HomeAttentionCell class] forCellWithReuseIdentifier:CellID];
        [_collectionView registerNib:[UINib nibWithNibName:ADCellID bundle:nil] forCellWithReuseIdentifier:ADCellID];
    }
    return _collectionView;
}

-(UIScrollView *)categoryScrollView{
    if(!_categoryScrollView){
        _categoryScrollView=[[UIScrollView alloc]init];
    }
    return _categoryScrollView;
}

-(HomeCategoryView *)categoryView{
    if(!_categoryView){
        _categoryView=[[HomeCategoryView alloc]init];
        _categoryView.backgroundColor = [UIColor whiteColor];
    }
    return _categoryView;
}

-(void)viewWillAppear:(BOOL)animated{
    self.navigationController.navigationBarHidden=YES;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [self layoutUI];
    [self uploadContent];
}

#pragma mark - 布局
-(void)layoutUI{
    [self.view addSubview:self.collectionView];
    [self.view addSubview:self.categoryScrollView];
    [self.view addSubview:self.categoryView];
    
    [self.collectionView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.view).offset(SafeAreaTopHeight);
        make.bottom.equalTo(self.view).offset(-TabBarHeight);
        make.left.equalTo(self.view);
        make.width.mas_equalTo(MYScreenWidth);
    }];
    [self.categoryScrollView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.view).offset(SafeAreaTopHeight);
        make.left.equalTo(self.view);
        make.width.mas_equalTo(MYScreenWidth);
        make.height.mas_equalTo(CategoryViewH);
    }];
    
    [self.categoryView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.edges.equalTo(self.categoryScrollView);
    }];
}

#pragma mark - 接口调用
-(void)uploadContent{
    //test
    NSArray * arr = @[@"全部",@"行业资讯",@"优秀图集",@"小视频"];
    self.categoryView.categoryArr = arr;
    
    [self.paramaters setObject:[NSNumber numberWithInt:1] forKey:@"page"];
    [ServiceAPIManager ServiceAPIManagerForGETWithOutHUD:ListUrl parameters:self.paramaters success:^(id  _Nonnull json) {
        if([json[@"errorCode"] intValue]==0){
            NSArray * arr = json[@"data"][@"rows"];
            [self.listArray removeAllObjects];
            if(arr.count>0){
                for(int i = 0;i<arr.count;i++){
                    ArticleListModel * model = [[ArticleListModel alloc]initWithDictionary:arr[i] error:nil];
                    [self.listArray addObject:model];
                }
            }
            dispatch_async(dispatch_get_main_queue(), ^{
                [self.collectionView reloadData];
            });
            [self.collectionView.mj_header endRefreshing];
        }else{
            [SVProgressHUD showErrorWithStatus:json[@"msg"]];
        }
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        
    }];
}

-(void)loadContent{
    NSNumber *page = self.paramaters[@"page"];
    int pages =[page intValue];
    pages++;
    [self.paramaters setObject:[NSNumber numberWithInt:pages] forKey:@"page"];
    [ServiceAPIManager ServiceAPIManagerForGETWithOutHUD:ListUrl parameters:self.paramaters success:^(id  _Nonnull json) {
        if([json[@"errorCode"] intValue]==0){
            NSArray * arr = json[@"data"][@"rows"];
            if(arr.count>0){
                for(int i = 0;i<arr.count;i++){
                    ArticleListModel * model = [[ArticleListModel alloc]initWithDictionary:arr[i] error:nil];
                    [self.listArray addObject:model];
                }
            }
            [self.collectionView reloadData];
            [self.collectionView.mj_footer endRefreshing];
            [self.collectionView.mj_footer resetNoMoreData];
        }else{
            [self.collectionView.mj_footer endRefreshingWithNoMoreData];
        }
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        
    }];
}

#pragma mark - UICollectionViewDataSource
- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section{
    return self.listArray.count;
}

- (NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView {
    return 1;
}

- ( UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath{
    UICollectionViewCell * cell = nil;
    ArticleListModel * model=self.listArray[indexPath.row];
    if([model.isAd intValue]){
        HomeAdCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:ADCellID forIndexPath:indexPath];
        cell.selectedBackgroundView = [[UIView alloc] initWithFrame:cell.frame];
        cell.selectedBackgroundView.backgroundColor = [UIColor clearColor];
        cell.backgroundColor = [UIColor whiteColor];
        return cell;
    }else{
        HomeAttentionCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:CellID forIndexPath:indexPath];
        cell.selectedBackgroundView = [[UIView alloc] initWithFrame:cell.frame];
        cell.selectedBackgroundView.backgroundColor = [UIColor clearColor];
        cell.model = model;
        cell.backgroundColor = [UIColor whiteColor];
//        [self setCellStyle:cell];
        return cell;
    }
    return cell;
}

-(void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath{
    ArticleDetailController * vc = [[ArticleDetailController alloc]init];
    [self.navigationController pushViewController:vc animated:YES];
}

#pragma mark - UICollectionViewDelegateFlowLayout
-(UIEdgeInsets)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout insetForSectionAtIndex:(NSInteger)section
{
    return UIEdgeInsetsMake(CategoryViewH,0,10,0);
}

- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath{
    ArticleListModel * model=self.listArray[indexPath.row];
    CGFloat cellHeight=[model.cellHeight floatValue];
    if (cellHeight == 0.f) {
        cellHeight += 170;//基础高度(不算content)
        NSString * text = model.content;
        NSMutableDictionary * dic = [NSMutableDictionary dictionary];
        dic[NSFontAttributeName] = LLFFontPingFangSC(14);
        CGRect textRect = [text boundingRectWithSize:CGSizeMake(MYScreenWidth-20, MAXFLOAT)  options:NSStringDrawingUsesLineFragmentOrigin attributes:dic context:nil];
        cellHeight += textRect.size.height;
        if(model.coverImg.count>0){
            if(model.coverImg.count==1){
                cellHeight+=MYScreenWidth*0.7;
            }else if(model.coverImg.count==2){
                cellHeight+=MYScreenWidth*0.4;
            }else{
                cellHeight+=(model.coverImg.count/3+1)*(((MYScreenWidth-20)/3)-7.5);
            }
            cellHeight-=20;
        }
        model.cellHeight = [NSString stringWithFormat:@"%d",(int)cellHeight];
        return CGSizeMake(MYScreenWidth,[model.cellHeight floatValue]);
    }
     return CGSizeMake(MYScreenWidth, MYScreenHeight*0.2);
}

#pragma mark - SPPageDelegate
-(UIScrollView *)preferScrollView
{
    return self.scrollView;
}

#pragma mark - 视图滚动监听
-(void)scrollViewDidScroll:(UIScrollView *)scrollView{
    if(self.collectionView){
        if(scrollView.contentOffset.y/2>0){
            [UIView animateWithDuration:0.1 animations:^{
                [self.categoryScrollView mas_updateConstraints:^(MASConstraintMaker *make) {
                    make.top.equalTo(self.view).offset(SafeAreaTopHeight-scrollView.contentOffset.y/4);
                }];
            }];
        }
    }
}

@end
