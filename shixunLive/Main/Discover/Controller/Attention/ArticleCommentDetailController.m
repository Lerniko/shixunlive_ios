//
//  ArticleCommentReplyController.m
//  shixunLive
//
//  Created by apple on 2020/1/6.
//  Copyright © 2020 MSEducation. All rights reserved.
//

#import "ArticleCommentDetailController.h"
#import "ArticleCommentDetailCell.h"
#import "ArticleCommentDetailCommentView.h"

#define BottomViewH 56
#define CellID @"ArticleCommentDetailCell"
@interface ArticleCommentDetailController ()<UITableViewDelegate,UITableViewDataSource>

@property(nonatomic,strong)NSMutableArray * listArray;

@property(nonatomic,strong)UITableView * tableView;

@property(nonatomic,strong)ArticleCommentDetailCommentView * bottomView;

@end

@implementation ArticleCommentDetailController
#pragma mark - 数据
-(NSMutableArray *)listArray{
    if(!_listArray){
        _listArray = [NSMutableArray new];
    }
    return _listArray;
}

#pragma mark - UI
-(UITableView *)tableView{
    if(!_tableView){
        _tableView = [[UITableView alloc]initWithFrame:CGRectMake(0, 0, MYScreenWidth, MYScreenHeight) style:UITableViewStylePlain];
        _tableView.delegate=self;
        _tableView.dataSource=self;
        _tableView.scrollEnabled=NO;
        _tableView.backgroundColor = [UIColor whiteColor];
        _tableView.tableFooterView = [[UIView alloc]initWithFrame:CGRectZero];
        [_tableView registerClass:[ArticleCommentDetailCell class] forCellReuseIdentifier:CellID];
    }
    return _tableView;
}

-(ArticleCommentDetailCommentView *)bottomView{
    if(!_bottomView){
        _bottomView=[[NSBundle mainBundle]loadNibNamed:@"ArticleCommentDetailCommentView" owner:self options:nil].firstObject;
    }
    return _bottomView;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.title = @"回复详情";
    self.view.backgroundColor = [UIColor whiteColor];
    [self setNavBar];
    [self layoutUI];
}

#pragma mark - 布局
-(void)setNavBar{
    self.navigationController.navigationBarHidden=NO;
    UIButton *leftBtn = [[UIButton alloc]initWithFrame:CGRectMake(0, 0, 30, 30)];
    [leftBtn setImage:[UIImage imageNamed:@"icon_comeBack"] forState:0];
    leftBtn.titleLabel.font = [UIFont systemFontOfSize:10];
    [leftBtn addTarget:self action:@selector(comeBack) forControlEvents:UIControlEventTouchUpInside];
    UIBarButtonItem *leftItem = [[UIBarButtonItem alloc]initWithCustomView:leftBtn];
    self.navigationItem.leftBarButtonItem = leftItem;
}

-(void)layoutUI{
    [self.view addSubview:self.tableView];
    [self.view addSubview:self.bottomView];
    [self.tableView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.view).offset(SafeAreaTopHeight);
        make.left.equalTo(self.view);
        make.width.mas_equalTo(MYScreenWidth);
        if(KisiOS11){
            make.bottom.equalTo(self.view.mas_safeAreaLayoutGuideBottom).offset(-BottomViewH);
        }else{
            make.bottom.equalTo(self.view).offset(-BottomViewH);
        }
    }];
    
    [self.bottomView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.right.equalTo(self.view);
        make.height.mas_equalTo(BottomViewH);
        if(KisiOS11){
            make.bottom.equalTo(self.view.mas_safeAreaLayoutGuideBottom);
        }else{
            make.bottom.equalTo(self.view);
        }
    }];
}

#pragma mark - UITableViewDelegate
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return 2;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    ArticleCommentDetailCell *cell = [tableView dequeueReusableCellWithIdentifier:CellID];
    //    cell.model =self.listArray[indexPath.row];
    cell.selectedBackgroundView = [[UIView alloc] initWithFrame:cell.frame];
    cell.selectedBackgroundView.backgroundColor = [UIColor clearColor];
    return cell;
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    //    ExpressSingleInfoModel * model=self.listArray[indexPath.row];
    //    CGFloat cellHeight=[model.cellHeight floatValue];
    //    if (cellHeight == 0.f) {    // 如果字典中没有存储cell的高度
    //        cellHeight += 50;
    //        NSString * text = model.status;
    //        NSMutableDictionary * dic = [NSMutableDictionary dictionary];
    //        dic[NSFontAttributeName] = [UIFont systemFontOfSize:13];
    //        CGRect textRect = [text boundingRectWithSize:CGSizeMake((MYScreenWidth-50), MAXFLOAT)  options:NSStringDrawingUsesLineFragmentOrigin attributes:dic context:nil];
    //        cellHeight += textRect.size.height;
    //        model.cellHeight = [NSString stringWithFormat:@"%d",(int)cellHeight];
    //    }
    //    return cellHeight;
    return 123;
}

#pragma mark - 按钮事件
-(void)comeBack{
    [self.navigationController popViewControllerAnimated:YES];
}

@end
