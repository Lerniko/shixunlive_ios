//
//  HomeCommendController.h
//  shixunLive
//
//  Created by apple on 2019/12/31.
//  Copyright © 2019 MSEducation. All rights reserved.
//

#import "SPPageProtocol.h"

NS_ASSUME_NONNULL_BEGIN

@interface HomeCommendController : UIViewController<SPPageSubControllerDataSource>

@property (nonatomic,copy) void (^HomeCommendControllerBlock)(BOOL BlackViewBeClick);

@end

NS_ASSUME_NONNULL_END
