//
//  HomeCommendController.m
//  shixunLive
//
//  Created by apple on 2019/12/31.
//  Copyright © 2019 MSEducation. All rights reserved.
//

#import "HomeCommendController.h"
#import "OUNavigationController.h"
#import "HomeCategoryView.h"
#import "OJLWaterLayout.h"

#import "PhotoWallListCell.h"

#import "PhotoWallModel.h"

#define CategoryViewH 40
#define CellID @"PhotoWallListCell"
#define ListUrl @"/portal/atlas/list"
@interface HomeCommendController ()<UICollectionViewDelegate,UICollectionViewDataSource,OJLWaterLayoutDelegate>

@property (nonatomic,strong)UIScrollView * scrollView;
///分类
@property (nonatomic,strong)UIScrollView * categoryScrollView;
@property (nonatomic,strong)HomeCategoryView * categoryView;
///主视图
@property (nonatomic,strong)UICollectionView * collectionView;
@property (nonatomic,strong)OJLWaterLayout* layout;
///参数
@property (nonatomic,strong)NSMutableDictionary * paramaters;
///数据
@property (nonatomic,strong)NSMutableArray * listArray;

@end

@implementation HomeCommendController
#pragma mark - 模型
-(NSMutableArray *)listArray{
    if(!_listArray){
        _listArray=[NSMutableArray new];
    }
    return _listArray;
}

-(NSMutableDictionary *)paramaters{
    if(!_paramaters){
        _paramaters=[NSMutableDictionary new];
    }
    return _paramaters;
}

#pragma mark - UI
-(UICollectionView *)collectionView{
    if(!_collectionView){
        OJLWaterLayout* layout = [[OJLWaterLayout alloc] init];
        self.layout = layout;
        layout.numberOfCol = 2;
        layout.rowPanding = 15;
        layout.colPanding = 15;
        layout.sectionInset = UIEdgeInsetsMake(CategoryViewH,10,10,10);
        layout.delegate = self;
        [layout autuContentSize];
        
        _collectionView = [[UICollectionView alloc] initWithFrame:CGRectMake(0, SafeAreaTopHeight, MYScreenWidth, MYScreenHeight) collectionViewLayout:layout];
        _collectionView.dataSource = self;
        _collectionView.delegate = self;
        _collectionView.backgroundColor = LLFColor(245,245,245);
        _collectionView.mj_header = [MJRefreshNormalHeader headerWithRefreshingTarget:self refreshingAction:@selector(uploadContent)];
        _collectionView.mj_footer = [MJRefreshBackNormalFooter footerWithRefreshingTarget:self refreshingAction:@selector(loadContent)];
        [_collectionView registerNib:[UINib nibWithNibName:CellID bundle:nil] forCellWithReuseIdentifier:CellID];
    }
    return _collectionView;
}

-(UIScrollView *)categoryScrollView{
    if(!_categoryScrollView){
        _categoryScrollView=[[UIScrollView alloc]init];
    }
    return _categoryScrollView;
}

-(HomeCategoryView *)categoryView{
    if(!_categoryView){
        _categoryView=[[HomeCategoryView alloc]init];
        _categoryView.backgroundColor = [UIColor whiteColor];
    }
    return _categoryView;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [self layoutUI];
    [self uploadContent];
}

#pragma mark - SSPageDelegate
-(UIScrollView *)preferScrollView
{
    return self.scrollView;
}

#pragma mark - 布局
-(void)layoutUI{
    [self.view addSubview:self.collectionView];
    [self.view addSubview:self.categoryView];
    [self.view addSubview:self.categoryScrollView];
    [self.collectionView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.view).offset(SafeAreaTopHeight);
        make.bottom.equalTo(self.view).offset(-TabBarHeight);
        make.left.equalTo(self.view);
        make.width.mas_equalTo(MYScreenWidth);
    }];
    
    [self.categoryScrollView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.view).offset(SafeAreaTopHeight);
        make.left.equalTo(self.view);
        make.width.mas_equalTo(MYScreenWidth);
        make.height.mas_equalTo(CategoryViewH);
    }];
    
    [self.categoryView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.edges.equalTo(self.categoryScrollView);
    }];
}

#pragma mark - 加载数据
-(void)uploadContent{
    NSArray * arr2 = @[@"全部",@"东西",@"都",@"没有"];
    self.categoryView.categoryArr = arr2;
    
    [self.paramaters setObject:[NSNumber numberWithInt:1] forKey:@"page"];
    [ServiceAPIManager ServiceAPIManagerForGETWithOutHUD:ListUrl parameters:self.paramaters success:^(id  _Nonnull json) {
        if([json[@"errorCode"] intValue]==0){
        NSArray * arr = json[@"data"][@"rows"];
        if(arr.count>0){
            [self.listArray removeAllObjects];
            for(int i = 0;i<arr.count;i++){
                    PhotoWallListModel * model = [[PhotoWallListModel alloc]initWithDictionary:arr[i] error:nil];
                    UIImage * image = [UIImage imageWithData:[NSData dataWithContentsOfURL:[NSURL URLWithString:model.imgList[0]]]];
                    model.h = [NSString stringWithFormat:@"%d",image.size.height];
                    model.w = [NSString stringWithFormat:@"%d",image.size.width];
                    [self.listArray addObject:model];
                }
                
                dispatch_async(dispatch_get_main_queue(), ^{
                    [UIView performWithoutAnimation:^{
                        //刷新界面
                        [self.collectionView reloadData];
                    }];
                    [[CommonCodeManager shareManager]hiddenLoadingView];
                    [self.collectionView.mj_header endRefreshing];
                });
            }
        }else{
            dispatch_async(dispatch_get_main_queue(), ^{
                [UIView performWithoutAnimation:^{
                    //刷新界面
                    [self.collectionView reloadData];
                }];
                [[CommonCodeManager shareManager]hiddenLoadingView];
                [self.collectionView.mj_header endRefreshing];
            });
        }
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        
    }];
}

-(void)loadContent{
    NSNumber *page = self.paramaters[@"page"];
    int pages =[page intValue];
    pages++;
    [self.paramaters setObject:[NSNumber numberWithInt:pages] forKey:@"page"];
    [ServiceAPIManager ServiceAPIManagerForGETWithOutHUD:ListUrl parameters:self.paramaters success:^(id  _Nonnull json) {
        if([json[@"errorCode"] intValue]==0){
            NSArray * arr = json[@"data"][@"rows"];
            if(arr.count>0){
                for(int i = 0;i<arr.count;i++){
                    PhotoWallListModel * model = [[PhotoWallListModel alloc]initWithDictionary:arr[i] error:nil];
                    UIImage * image = [UIImage imageWithData:[NSData dataWithContentsOfURL:[NSURL URLWithString:model.imgList[0]]]];
                    model.h = [NSString stringWithFormat:@"%d",image.size.height];
                    model.w = [NSString stringWithFormat:@"%d",image.size.width];
                    [self.listArray addObject:model];
                }
                [self.collectionView reloadData];
                [self.collectionView.mj_footer endRefreshing];
                [self.collectionView.mj_footer resetNoMoreData];
            }else{
                [self.collectionView.mj_footer endRefreshingWithNoMoreData];
            }
        }else{
            [SVProgressHUD showErrorWithStatus:json[@"msg"]];
        }
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        
    }];
}

#pragma mark - UICollectionViewDataSource
- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section{
    return self.listArray.count;
}

- (NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView {
    return 1;
}

- ( UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath{
    PhotoWallListCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:CellID forIndexPath:indexPath];
    cell.selectedBackgroundView = [[UIView alloc] initWithFrame:cell.frame];
    cell.selectedBackgroundView.backgroundColor = [UIColor clearColor];
    cell.model = self.listArray[indexPath.row];
    cell.layer.cornerRadius=10;
    cell.layer.masksToBounds=YES;
    return cell;
}

#pragma mark OJLWaterLayoutDelegate
-(CGFloat)OJLWaterLayout:(OJLWaterLayout *)OJLWaterLayout itemHeightForIndexPath:(NSIndexPath *)indexPath{
    PhotoWallListModel* model = self.listArray[indexPath.item];
    CGFloat width = ([UIScreen mainScreen].bounds.size.width - self.layout.sectionInset.left - self.layout.sectionInset.right - (self.layout.colPanding * (self.layout.numberOfCol - 1))) / self.layout.numberOfCol;
    CGFloat scale = [model.w floatValue] / width;
    CGFloat height = [model.h floatValue] / scale;
    return height;
}

-(void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath{
//    PhotoWallListModel* model = self.listArray[indexPath.item];
//    PhotoWallListCell* cell = (PhotoWallListCell*)[collectionView cellForItemAtIndexPath:indexPath];
//    CGRect desImageViewRect = CGRectMake(0, 60 + 64, [model scaleSize].width, [model scaleSize].height);

    //    DetailViewController* vc = [[DetailViewController alloc] initWithModel:model desImageViewRect:desImageViewRect];
//    [((OUNavigationController*) self.navigationController) pushViewController:vc withImageView:cell.imageView desRect:desImageViewRect delegate:vc];
}

#pragma mark - 视图滚动监听
-(void)scrollViewDidScroll:(UIScrollView *)scrollView{
    if(self.collectionView){
        if(scrollView.contentOffset.y/2>0){
            [UIView animateWithDuration:0.1 animations:^{
                [self.categoryScrollView mas_updateConstraints:^(MASConstraintMaker *make) {
                    make.top.equalTo(self.view).offset(SafeAreaTopHeight-scrollView.contentOffset.y/4);
                }];
            }];
        }
    }
}

@end
