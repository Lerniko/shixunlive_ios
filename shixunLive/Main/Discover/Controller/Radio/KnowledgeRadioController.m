//
//  KnowledgeRadioController.m
//  shixunLive
//
//  Created by apple on 2020/1/3.
//  Copyright © 2020 MSEducation. All rights reserved.
//

#import "KnowledgeRadioController.h"
#import "DCCycleScrollView.h"

@interface KnowledgeRadioController ()<DCCycleScrollViewDelegate>

@property (nonatomic,strong)UIScrollView * scrollView;

@end

@implementation KnowledgeRadioController

- (void)viewDidLoad {
    [super viewDidLoad];
    
//    NSArray *imageArr = @[@"h1.jpg",
//                          @"h2.jpg",
//                          @"h3.jpg",
//                          @"h4.jpg",
//                          ];
//    DCCycleScrollView *banner = [DCCycleScrollView cycleScrollViewWithFrame:CGRectMake(0, 100, self.view.frame.size.width, 135) shouldInfiniteLoop:NO imageGroups:imageArr];
//    //    banner.placeholderImage = [UIImage imageNamed:@"placeholderImage"];
//    //    banner.cellPlaceholderImage = [UIImage imageNamed:@"placeholderImage"];
//    banner.autoScrollTimeInterval = 5;
//    banner.autoScroll = YES;
//    banner.isZoom = YES;
//    banner.itemSpace = 0;
//    banner.imgCornerRadius = 10;
//    banner.itemWidth = self.view.frame.size.width - 100;
//    banner.delegate = self;
//    [self.view addSubview:banner];
}

#pragma mark - DCCycleScrollViewDelegate
-(void)cycleScrollView:(DCCycleScrollView *)cycleScrollView didSelectItemAtIndex:(NSInteger)index
{
    NSLog(@"index = %ld",(long)index);
}

#pragma mark - 控件
-(UIScrollView *)preferScrollView
{
    return self.scrollView;
}

@end
