//
//  NSXLearnViewController.m
//  shixunLive
//
//  Created by apple on 2019/12/31.
//  Copyright © 2019 MSEducation. All rights reserved.
//

#import "NSXLearnViewController.h"
#import "LearnStudentStudyController.h"
#import "PreschoolKnowledgeController.h"
#import "KnowledgeRadioController.h"
#import "StudyDatumController.h"
#import "LearnHeaderView.h"

@interface NSXLearnViewController ()<LearnHeaderViewDelegate>

@property (nonatomic,strong)LearnHeaderView * headerView;

@end

@implementation NSXLearnViewController
#pragma mark - UI
-(LearnHeaderView *)headerView{
    if(!_headerView){
        _headerView=[[NSBundle mainBundle]loadNibNamed:@"LearnHeaderView" owner:self options:nil].firstObject;
        _headerView.delegate=self;
    }
    return _headerView;
}

-(void)viewWillAppear:(BOOL)animated{
    self.navigationController.navigationBarHidden=YES;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [self layoutUI];
    CurrentControlPage(@"Learn");
    [LLFNotiCenter addObserver:self selector:@selector(changePage:) name:@"changeLearnCurrentSel" object:nil];
}

#pragma mark - 布局
-(void)layoutUI{
    [self.view addSubview:self.headerView];
    [self.headerView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.left.right.equalTo(self.view);
        make.height.mas_equalTo(SafeAreaTopHeight);
    }];
}

#pragma mark - HomeHeaderViewDelegate
-(void)pageControlBtnClickWithIndex:(NSInteger)index{
    
}

#pragma mark - SPpageController
- (UIViewController *)controllerAtIndex:(NSInteger)index
{
    if (index == 0) {
        LearnStudentStudyController *coverController = [[LearnStudentStudyController alloc] init];
        coverController.view.backgroundColor = [UIColor whiteColor];
        coverController.view.frame = [self preferPageFrame];
        coverController.LearnStudentControllerBlock = ^(BOOL isTap){
            
        };
        return coverController;
    } else if (index == 1) {
        PreschoolKnowledgeController *coverController = [[PreschoolKnowledgeController alloc] init];
        coverController.view.backgroundColor = [UIColor whiteColor];
        coverController.view.frame = [self preferPageFrame];
        coverController.PreschoolKnowledgeControllerBlock = ^(BOOL isTap){
            
        };
        return coverController;
    } else if(index == 2) {
        KnowledgeRadioController *coverController = [[KnowledgeRadioController alloc] init];
        coverController.view.backgroundColor = [UIColor whiteColor];
        coverController.view.frame = [self preferPageFrame];
        coverController.KnowledgeRadioControllerBlock = ^(BOOL isTap){
            
        };
        return coverController;
    }
        StudyDatumController *coverController = [[StudyDatumController alloc] init];
        coverController.view.backgroundColor = [UIColor whiteColor];
        coverController.view.frame = [self preferPageFrame];
        coverController.StudyDatumControllerBlock = ^(BOOL isTap){
            
        };
    return coverController;
}

- (NSString *)titleForIndex:(NSInteger)index
{
    return [NSString stringWithFormat:@"TAB%zd", index];
}

-(BOOL)isSubPageCanScrollForIndex:(NSInteger)index
{
    return YES;
}

- (BOOL)needMarkView
{
    return YES;
}

- (NSInteger)numberOfControllers
{
    return 4;
}

-(BOOL)isPreLoad {
    return NO;
}

#pragma mark - 通知
-(void)changePage:(NSNotification *)notifi{
    NSNumber * page = [notifi object];
    [self.headerView changePageWithIndex:page.intValue];
}

@end
