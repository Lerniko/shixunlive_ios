//
//  NSXHomeViewController.m
//  shixunLive
//
//  Created by apple on 2019/12/31.
//  Copyright © 2019 MSEducation. All rights reserved.
//

#import "NSXHomeViewController.h"
#import "HomeAttentionController.h"
#import "HomeCommendController.h"
#import "HomeDiscoverController.h"
#import "SPPageController.h"
#import "HomeHeaderView.h"

@interface NSXHomeViewController ()<HomeHeaderViewDelegate>

@property (nonatomic,strong)HomeHeaderView * headerView;

@property (nonatomic,strong)UIButton * functionBtn;

@end

@implementation NSXHomeViewController
#pragma mark - UI
-(HomeHeaderView *)headerView{
    if(!_headerView){
        _headerView=[[NSBundle mainBundle]loadNibNamed:@"HomeHeaderView" owner:self options:nil].firstObject;
        _headerView.delegate=self;
    }
    return _headerView;
}

-(UIButton *)functionBtn{
    if(!_functionBtn){
        _functionBtn=[UIButton buttonWithType:0];
        [_functionBtn setImage:[UIImage imageNamed:@"icon_home_submit"] forState:0];
        _functionBtn.layer.cornerRadius=24;
        _functionBtn.layer.masksToBounds=YES;
        [_functionBtn addTarget:self action:@selector(functionBtnClick) forControlEvents:UIControlEventTouchUpInside];
    }
    return _functionBtn;
}

-(void)viewWillAppear:(BOOL)animated{
    self.navigationController.navigationBarHidden=YES;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [self layoutUI];
    
    CurrentControlPage(@"Home");
    [LLFNotiCenter addObserver:self selector:@selector(changePage:) name:@"changeHomeCurrentSel" object:nil];
}

#pragma mark - 布局
-(void)layoutUI{
    [self.view addSubview:self.headerView];
    [self.headerView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.left.right.equalTo(self.view);
        make.height.mas_equalTo(SafeAreaTopHeight);
    }];
    
    [self.view addSubview:self.functionBtn];
    [self.functionBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.equalTo(self.view).offset(-12);
        make.height.width.mas_equalTo(48);
        if(KisiOS11){
            make.bottom.equalTo(self.view.mas_safeAreaLayoutGuideBottom).offset(-12);
        }else{
            make.bottom.equalTo(self.view).offset(-12);
        }
    }];
}

#pragma mark - HomeHeaderViewDelegate
-(void)pageControlBtnClickWithIndex:(NSInteger)index{
    
}

#pragma mark - SPpageController
- (UIViewController *)controllerAtIndex:(NSInteger)index
{
    if (index == 0) {
        HomeAttentionController *coverController = [[HomeAttentionController alloc] init];
        coverController.view.backgroundColor = LLFColor(240,240,240);
        coverController.view.frame = [self preferPageFrame];
        coverController.HomeAttentionControllerBlock = ^(BOOL isTap){
            
        };
        return coverController;
    } else if (index == 1) {
        HomeCommendController *coverController = [[HomeCommendController alloc] init];
        coverController.view.backgroundColor = LLFColor(240,240,240);
        coverController.view.frame = [self preferPageFrame];
        coverController.HomeCommendControllerBlock = ^(BOOL isTap){
            
        };
        return coverController;
    }
        HomeDiscoverController *coverController = [[HomeDiscoverController alloc] init];
        coverController.view.backgroundColor = LLFColor(240,240,240);
        coverController.view.frame = [self preferPageFrame];
        coverController.HomeDiscoverControllerBlock = ^(BOOL isTap){
            
        };
        return coverController;
}

- (NSString *)titleForIndex:(NSInteger)index
{
    return [NSString stringWithFormat:@"TAB%zd", index];
}

-(BOOL)isSubPageCanScrollForIndex:(NSInteger)index
{
    return YES;
}

- (BOOL)needMarkView
{
    return YES;
}

- (NSInteger)numberOfControllers
{
    return 3;
}

-(BOOL)isPreLoad {
    return NO;
}

#pragma mark - 通知
-(void)changePage:(NSNotification *)notifi{
    NSNumber * page = [notifi object];
    [self.headerView changePageWithIndex:page.intValue];
}

#pragma mark - 按钮事件
-(void)functionBtnClick{
    
}

@end
