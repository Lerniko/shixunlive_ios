//
//  LearnReportDetailController.m
//  shixunLive
//
//  Created by apple on 2020/1/6.
//  Copyright © 2020 MSEducation. All rights reserved.
//

#import "LearnReportDetailController.h"
#import "LearnStudyReportView.h"
#import "LearnReportDetailSelectView.h"
#import "LearnReportTitleView.h"

#import "LearnReportCourseCell.h"
#import "LearnReportSoundCell.h"

#define CourseCellID @"LearnReportCourseCell"
#define SoundCellID @"LearnReportSoundCell"
@interface LearnReportDetailController ()<UITableViewDelegate,UITableViewDataSource>
//学习时长
@property(nonatomic,strong)LearnStudyReportView * reportView;
//最近学习
@property(nonatomic,strong)LearnReportTitleView * titleView;
//选择器
@property(nonatomic,strong)LearnReportDetailSelectView * selectView;
//列表
@property(nonatomic,strong)UITableView * tableView;
//主视图
@property(nonatomic,strong)UIView * headerView;

@end

@implementation LearnReportDetailController

#pragma mark - UI
-(LearnStudyReportView *)reportView{
    if(!_reportView){
        _reportView=[[NSBundle mainBundle]loadNibNamed:@"LearnStudyReportView" owner:self options:nil].firstObject;
    }
    return _reportView;
}

-(LearnReportTitleView *)titleView{
    if(!_titleView){
        _titleView=[[NSBundle mainBundle]loadNibNamed:@"LearnReportTitleView" owner:self options:nil].firstObject;
    }
    return _titleView;
}

-(LearnReportDetailSelectView *)selectView{
    if(!_selectView){
        _selectView=[[LearnReportDetailSelectView alloc]init];
        _selectView.titleArray = @[@"课程",@"音频"];
    }
    return _selectView;
}

-(UIView *)headerView{
    if(!_headerView){
        _headerView=[[UIView alloc]init];
        _headerView.backgroundColor = [UIColor whiteColor];
    }
    return _headerView;
}

-(UITableView *)tableView{
    if(!_tableView){
        _tableView = [[UITableView alloc]initWithFrame:CGRectMake(0, 0, MYScreenWidth, MYScreenHeight) style:UITableViewStyleGrouped];
        _tableView.delegate=self;
        _tableView.dataSource=self;
        _tableView.backgroundColor = [UIColor whiteColor];
        WEAKSELF;
        _tableView.mj_footer = [MJRefreshAutoNormalFooter footerWithRefreshingBlock:^{
            [weakSelf.tableView reloadData];
            [weakSelf.tableView.mj_footer endRefreshingWithNoMoreData];
        }];
    }
    return _tableView;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    self.title = @"学习报告";
    self.view.backgroundColor = [UIColor whiteColor];
    [self setNavBar];
    [self layoutUI];
}

#pragma mark - 布局
-(void)setNavBar{
    self.navigationController.navigationBarHidden=NO;
    UIButton *leftBtn = [[UIButton alloc]initWithFrame:CGRectMake(0, 0, 30, 30)];
    [leftBtn setImage:[UIImage imageNamed:@"icon_comeBack"] forState:0];
    leftBtn.titleLabel.font = [UIFont systemFontOfSize:10];
    [leftBtn addTarget:self action:@selector(comeBack) forControlEvents:UIControlEventTouchUpInside];
    UIBarButtonItem *leftItem = [[UIBarButtonItem alloc]initWithCustomView:leftBtn];
    self.navigationItem.leftBarButtonItem = leftItem;
    
    UIButton *rightBtn = [[UIButton alloc]initWithFrame:CGRectMake(0, 0, 30, 30)];
    [rightBtn setImage:[UIImage imageNamed:@"icon_home_search"] forState:0];
    rightBtn.titleLabel.font = [UIFont systemFontOfSize:10];
    [rightBtn addTarget:self action:@selector(searchBtnClick) forControlEvents:UIControlEventTouchUpInside];
    UIBarButtonItem *rightItem = [[UIBarButtonItem alloc]initWithCustomView:rightBtn];
    self.navigationItem.rightBarButtonItem = rightItem;
}

-(void)layoutUI{
    [self.view addSubview:self.tableView];
    [self.view addSubview:self.headerView];
    [self.headerView addSubview:self.reportView];
    [self.headerView addSubview:self.titleView];
    [self.headerView addSubview:self.selectView];
    
    [self.tableView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.width.equalTo(self.view);
        make.height.equalTo(self.view);
    }];
    
    [self.headerView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.view).offset(SafeAreaTopHeight);
        make.left.equalTo(self.view);
        make.width.mas_equalTo(MYScreenWidth);
    }];
    
    [self.reportView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.headerView).offset(DefaultMargin);
        make.width.mas_equalTo(MYScreenWidth-DefaultMargin*2);
        make.centerX.equalTo(self.headerView);
        make.height.mas_equalTo(93);
    }];
    
    [self.titleView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.reportView.mas_bottom).offset(DefaultMargin);
        make.centerX.equalTo(self.headerView);
        make.height.mas_equalTo(50);
    }];
    
    [self.selectView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.titleView.mas_bottom).offset(DefaultMargin);
        make.centerX.equalTo(self.headerView);
        make.width.mas_equalTo(MYScreenWidth);
        make.height.mas_equalTo(40);
        make.bottom.equalTo(self.headerView);
    }];
}

#pragma mark - UITableViewDelegate
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return 10;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    LearnReportCourseCell* cell = nil;
    cell = [tableView dequeueReusableCellWithIdentifier:CourseCellID];
    if (cell == nil) {
        cell = [[NSBundle mainBundle] loadNibNamed:CourseCellID owner:nil options:nil].firstObject;
        cell.selectedBackgroundView = [[UIView alloc] initWithFrame:cell.frame];
        cell.selectedBackgroundView.backgroundColor = [UIColor clearColor];
        //        cell.model = self.categoryArray[indexPath.row];
    }
    return cell;
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    //    ExpressSingleInfoModel * model=self.listArray[indexPath.row];
    //    CGFloat cellHeight=[model.cellHeight floatValue];
    //    if (cellHeight == 0.f) {    // 如果字典中没有存储cell的高度
    //        cellHeight += 50;
    //        NSString * text = model.status;
    //        NSMutableDictionary * dic = [NSMutableDictionary dictionary];
    //        dic[NSFontAttributeName] = [UIFont systemFontOfSize:13];
    //        CGRect textRect = [text boundingRectWithSize:CGSizeMake((MYScreenWidth-50), MAXFLOAT)  options:NSStringDrawingUsesLineFragmentOrigin attributes:dic context:nil];
    //        cellHeight += textRect.size.height;
    //        model.cellHeight = [NSString stringWithFormat:@"%d",(int)cellHeight];
    //    }
    //    return cellHeight;
    return 80;
}

-(UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section{
    return self.headerView;
}

#pragma mark - 按钮事件
-(void)comeBack{
    [self.navigationController popViewControllerAnimated:YES];
}

-(void)searchBtnClick{
    
}

@end
