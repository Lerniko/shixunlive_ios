//
//  LearnStudentController.m
//  shixunLive
//
//  Created by apple on 2020/1/3.
//  Copyright © 2020 MSEducation. All rights reserved.
//

#import "LearnStudentStudyController.h"
#import "LearnReportDetailController.h"
#import "LearnStudyHeaderView.h"
#import "LearnStudySectionHeaderView.h"

#import "LearnStudyListenTableCell.h"
#import "LearnStudyPointCommendTableCell.h"
#import "LearnStudyDataCommendCell.h"

#define PointCommendCellH MYScreenHeight*0.27
#define DataCommendCellH 82
#define HeaderViewH 400
#define SectionHeaderH 40
#define ListenCellID @"LearnStudyListenTableCell"
#define PointCellID @"LearnStudyPointCommendTableCell"
#define DataCellID @"LearnStudyDataCommendCell"

#define SectionViewNibName @"LearnStudySectionHeaderView"
@interface LearnStudentStudyController ()<UITableViewDelegate,UITableViewDataSource,LearnStudyListenTableCellDelegate,LearnStudyPointCommendTableCellDelegate,LearnStudyHeaderViewDelegate>

@property (nonatomic,strong)UIScrollView * scrollView;
///头视图，学习报告以上
@property (nonatomic,strong)LearnStudyHeaderView * headerView;
///主视图
@property (nonatomic,strong)UITableView * tableView;
///数据
///每日一听
@property (nonatomic,strong)NSMutableArray * listenArr;
///必修
@property (nonatomic,strong)NSMutableArray * pointArr;
///资料
@property (nonatomic,strong)NSMutableArray * dataArr;

@end

@implementation LearnStudentStudyController

#pragma mark - 数据
-(NSMutableArray *)listenArr{
    if(!_listenArr){
        _listenArr=[NSMutableArray new];
    }
    return _listenArr;
}

-(NSMutableArray *)pointArr{
    if(!_pointArr){
        _pointArr=[NSMutableArray new];
    }
    return _pointArr;
}

-(NSMutableArray *)dataArr{
    if(!_dataArr){
        _dataArr=[NSMutableArray new];
    }
    return _dataArr;
}

#pragma mark - UI
-(LearnStudyHeaderView *)headerView{
    if(!_headerView){
        _headerView=[[LearnStudyHeaderView alloc]init];
        _headerView.delegate=self;
    }
    return _headerView;
}

-(UIScrollView *)scrollView{
    if(!_scrollView){
        _scrollView = [[UIScrollView alloc]init];
    }
    return _scrollView;
}

-(UITableView *)tableView{
    if(!_tableView){
        _tableView = [[UITableView alloc]initWithFrame:CGRectMake(0, 0, MYScreenWidth, MYScreenHeight) style:UITableViewStylePlain];
        _tableView.delegate=self;
        _tableView.dataSource=self;
        _tableView.tableHeaderView=self.headerView;
        _tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
        _tableView.tableFooterView = [[UIView alloc]initWithFrame:CGRectZero];
    }
    return _tableView;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [self layoutUI];
}

#pragma mark - 布局
-(void)layoutUI{
    [self.view addSubview:self.tableView];
    [self.tableView addSubview:self.headerView];
    
    [self.headerView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.left.equalTo(self.tableView);
        make.width.mas_equalTo(MYScreenWidth);
    }];
    
    [self.tableView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.view).offset(SafeAreaTopHeight);
        make.width.mas_equalTo(MYScreenWidth);
        make.left.bottom.equalTo(self.view);
    }];
}

#pragma mark - 加载数据
-(void)uploadContent{
    
}

#pragma mark - SPPageDelegate
-(UIScrollView *)preferScrollView
{
    return self.scrollView;
}

#pragma mark - LearnStudyHeaderViewDelegate
-(void)learnStudyViewCheckDetailReport{
    LearnReportDetailController * vc = [LearnReportDetailController new];
    [self.navigationController pushViewController:vc animated:YES];
}

#pragma mark ====== UITableViewDelegate ======
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    if(section==0){//每日一听
        return 1;
    }else if(section==1){//必修
        return 1;
    }//资料
    return 4;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    if(indexPath.section==0){//每日一听
        return 110;
    }else if(indexPath.section==1){//必修
        return (MYScreenHeight*0.23+20)*2;
    }//资料
    return DataCommendCellH;
}

-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return 3;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    if(indexPath.section==0){//每日一听
        [tableView registerClass:[LearnStudyListenTableCell class] forCellReuseIdentifier:ListenCellID];
        LearnStudyListenTableCell *cell = [tableView dequeueReusableCellWithIdentifier:ListenCellID forIndexPath:indexPath];
        cell.delegate = self;
        cell.listArr = self.listenArr;
        cell.backgroundColor = [UIColor clearColor];
        return cell;
    }else if(indexPath.section==1){//必修
        [tableView registerClass:[LearnStudyPointCommendTableCell class] forCellReuseIdentifier:PointCellID];
        LearnStudyPointCommendTableCell *cell = [tableView dequeueReusableCellWithIdentifier:PointCellID forIndexPath:indexPath];
        cell.delegate = self;
        cell.listArr = self.pointArr;
        return cell;
    }//资料
    LearnStudyDataCommendCell* cell = nil;
    cell = [tableView dequeueReusableCellWithIdentifier:DataCellID];
    if (cell == nil) {
        cell = [[NSBundle mainBundle] loadNibNamed:DataCellID owner:nil options:nil].firstObject;
        cell.selectedBackgroundView = [[UIView alloc] initWithFrame:cell.frame];
        cell.selectedBackgroundView.backgroundColor = [UIColor clearColor];
//        cell.model = self.categoryArray[indexPath.row];
    }
    return cell;
}

-(UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section{
    if(section==0){//每日一听
        LearnStudySectionHeaderView * headerView = [[NSBundle mainBundle]loadNibNamed:SectionViewNibName owner:self options:nil].firstObject;
        headerView.sectionType = LearnStudySectionHeaderTypeListen;
        return headerView;
    }else if(section==1){//必修
        LearnStudySectionHeaderView * headerView = [[NSBundle mainBundle]loadNibNamed:SectionViewNibName owner:self options:nil].firstObject;
        headerView.sectionType = LearnStudySectionHeaderTypePoint;
        return headerView;
    }//资料
    LearnStudySectionHeaderView * headerView = [[NSBundle mainBundle]loadNibNamed:SectionViewNibName owner:self options:nil].firstObject;
    headerView.sectionType = LearnStudySectionHeaderTypeData;
    return headerView;
}

-(CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
    return SectionHeaderH;
}

#pragma mark ====== SubTableViewCellDelegate ======
- (void)updateTableViewCellHeight:(LearnStudyListenTableCell *)cell andheight:(CGFloat)height andIndexPath:(NSIndexPath *)indexPath {
    [self.tableView reloadData];
//    if (![self.dicH[indexPath] isEqualToNumber:@(height)]) {
//        self.dicH[indexPath] = @(height);
//        [self.tableView reloadData];
//    }
}

//点击UICollectionViewCell的代理方法
- (void)didSelectItemAtIndexPath:(NSIndexPath *)indexPath withContent:(NSString *)content {
    
}

@end
