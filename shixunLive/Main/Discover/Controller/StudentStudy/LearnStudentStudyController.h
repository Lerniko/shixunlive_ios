//
//  LearnStudentController.h
//  shixunLive
//
//  Created by apple on 2020/1/3.
//  Copyright © 2020 MSEducation. All rights reserved.
//

#import "SPPageProtocol.h"

NS_ASSUME_NONNULL_BEGIN

@interface LearnStudentStudyController : UIViewController<SPPageSubControllerDataSource>

@property (nonatomic,copy) void (^LearnStudentControllerBlock)(BOOL BlackViewBeClick);

@end

NS_ASSUME_NONNULL_END
