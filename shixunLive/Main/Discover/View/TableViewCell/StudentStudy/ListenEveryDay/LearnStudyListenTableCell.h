//
//  LearnStudyListenTableCell.h
//  shixunLive
//
//  Created by apple on 2020/1/4.
//  Copyright © 2020 MSEducation. All rights reserved.
//

#import <UIKit/UIKit.h>

@class LearnStudyListenTableCell;
NS_ASSUME_NONNULL_BEGIN
@protocol LearnStudyListenTableCellDelegate <NSObject>

/**
 * 动态改变UITableViewCell的高度
 */
- (void)updateTableViewCellHeight:(LearnStudyListenTableCell *)cell andheight:(CGFloat)height andIndexPath:(NSIndexPath *)indexPath;


/**
 * 点击UICollectionViewCell的代理方法
 */
- (void)didSelectItemAtIndexPath:(NSIndexPath *)indexPath withContent:(NSString *)content;
@end

@interface LearnStudyListenTableCell : UITableViewCell

@property (nonatomic, weak) id<LearnStudyListenTableCellDelegate> delegate;

@property (nonatomic, strong) NSIndexPath *indexPath;
@property (nonatomic, strong) NSArray *listArr;


@end

NS_ASSUME_NONNULL_END
