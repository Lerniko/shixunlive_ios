//
//  LearnStudyPointCommendTableCell.m
//  shixunLive
//
//  Created by apple on 2020/1/4.
//  Copyright © 2020 MSEducation. All rights reserved.
//

#import "LearnStudyPointCommendTableCell.h"
#import "LearnStudyPointCommendCell.h"

#define CellH MYScreenHeight*0.23
#define CellW (MYScreenWidth-20-10)/2
#define CellID @"LearnStudyPointCommendCell"

@interface LearnStudyPointCommendTableCell ()<UICollectionViewDelegate, UICollectionViewDataSource>

@property (nonatomic,strong)UICollectionView *collectionView;
@property (nonatomic, assign) CGFloat heightED;

@end

@implementation LearnStudyPointCommendTableCell

- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier {
    self.heightED = 0;
    if (self = [super initWithStyle:style reuseIdentifier:reuseIdentifier]) {
        [self.contentView addSubview:self.collectionView];
        self.collectionView.frame = CGRectMake(0, 0,[UIScreen mainScreen].bounds.size.width, self.contentView.frame.size.height);
    }
    return self;
}

#pragma mark - UI
- (UICollectionView *)collectionView {
    if (!_collectionView) {
        UICollectionViewFlowLayout *layout = [[UICollectionViewFlowLayout alloc] init];
        layout.scrollDirection = UICollectionViewScrollDirectionVertical;
        layout.itemSize = CGSizeMake(CellW,CellH);
        layout.sectionInset = UIEdgeInsetsMake(10,10,10,10);
        
        _collectionView = [[UICollectionView alloc] initWithFrame:CGRectZero collectionViewLayout:layout];
        _collectionView.delegate = self;
        _collectionView.dataSource = self;
        [_collectionView registerNib:[UINib nibWithNibName:CellID bundle:nil] forCellWithReuseIdentifier:CellID];
        _collectionView.backgroundColor = [UIColor whiteColor];
        _collectionView.showsVerticalScrollIndicator=NO;
        _collectionView.scrollEnabled=NO;
    }
    return _collectionView;
}

#pragma mark ====== UICollectionViewDelegate ======
- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section {
    if (self.listArr.count == 0) {
        return 4;
    } else {
        return self.listArr.count;
    }
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath {
    LearnStudyPointCommendCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:CellID forIndexPath:indexPath];
    //    cell.textStr = self.listArr[indexPath.row];
    [self updateCollectionViewHeight:self.collectionView.collectionViewLayout.collectionViewContentSize.height];
    [self setCellStyle:cell];
    return cell;
}

- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath {
    if (self.delegate && [self.delegate respondsToSelector:@selector(didSelectItemAtIndexPath:withContent:)]) {
        [self.delegate didSelectItemAtIndexPath:indexPath withContent:self.listArr[indexPath.row]];
    }
}

- (void)updateCollectionViewHeight:(CGFloat)height {
    if (self.heightED != height) {
        self.heightED = height;
        self.collectionView.frame = CGRectMake(0,0, self.collectionView.frame.size.width, (CellH+20)*2);
        
        if (_delegate && [_delegate respondsToSelector:@selector(updateTableViewCellHeight:andheight:andIndexPath:)]) {
            [self.delegate updateTableViewCellHeight:self andheight:height andIndexPath:self.indexPath];
        }
    }
}

-(void)setListArr:(NSArray *)listArr{
    _listArr = listArr;
    self.heightED = 0;
}

-(void)setCellStyle:(LearnStudyPointCommendCell *)cell{
    cell.contentView.layer.backgroundColor = [UIColor whiteColor].CGColor;
    cell.contentView.layer.cornerRadius =8.0f;
    cell.contentView.layer.borderWidth =1.0f;
    cell.contentView.layer.borderColor = [UIColor clearColor].CGColor;
    cell.contentView.layer.masksToBounds =YES;
    cell.layer.shadowColor = LLFColor(238, 238, 238).CGColor;
    cell.layer.shadowOffset = CGSizeMake(0,2.0f);
    cell.layer.shadowRadius =10.0f;
    cell.layer.shadowOpacity =1.0f;
    cell.layer.masksToBounds =NO;
    cell.layer.cornerRadius=8.0f;
    cell.layer.shadowPath = [UIBezierPath bezierPathWithRoundedRect:cell.bounds cornerRadius:cell.contentView.layer.cornerRadius].CGPath;
}

@end
