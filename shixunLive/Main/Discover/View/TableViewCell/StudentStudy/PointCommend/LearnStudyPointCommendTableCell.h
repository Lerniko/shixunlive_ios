//
//  LearnStudyPointCommendTableCell.h
//  shixunLive
//
//  Created by apple on 2020/1/4.
//  Copyright © 2020 MSEducation. All rights reserved.
//

#import <UIKit/UIKit.h>

@class LearnStudyPointCommendTableCell;
NS_ASSUME_NONNULL_BEGIN
@protocol LearnStudyPointCommendTableCellDelegate <NSObject>

/**
 * 动态改变UITableViewCell的高度
 */
- (void)updateTableViewCellHeight:(LearnStudyPointCommendTableCell *)cell andheight:(CGFloat)height andIndexPath:(NSIndexPath *)indexPath;


/**
 * 点击UICollectionViewCell的代理方法
 */
- (void)didSelectItemAtIndexPath:(NSIndexPath *)indexPath withContent:(NSString *)content;
@end

@interface LearnStudyPointCommendTableCell : UITableViewCell

@property (nonatomic, weak) id<LearnStudyPointCommendTableCellDelegate> delegate;

@property (nonatomic, strong) NSIndexPath *indexPath;
@property (nonatomic, strong) NSArray *listArr;

@end

NS_ASSUME_NONNULL_END
