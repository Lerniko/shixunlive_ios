//
//  LearnHeaderView.h
//  shixunLive
//
//  Created by apple on 2020/1/3.
//  Copyright © 2020 MSEducation. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@protocol LearnHeaderViewDelegate <NSObject>

-(void)pageControlBtnClickWithIndex:(NSInteger)index;

@end

@interface LearnHeaderView : UIView

-(void)changePageWithIndex:(NSInteger)index;

@property(nonatomic,weak)id<LearnHeaderViewDelegate> delegate;

@end

NS_ASSUME_NONNULL_END
