//
//  LearnHeaderView.m
//  shixunLive
//
//  Created by apple on 2020/1/3.
//  Copyright © 2020 MSEducation. All rights reserved.
//

#import "LearnHeaderView.h"

#define BtnTag 120
#define UnderLineViewH 3
#define UnderLineViewW 75
@interface LearnHeaderView ()

@property (weak, nonatomic) IBOutlet UIButton *studentBtn;
@property (weak, nonatomic) IBOutlet UIButton *preschoolBtn;
@property (weak, nonatomic) IBOutlet UIButton *knowledgeRadioBtn;
@property (weak, nonatomic) IBOutlet UIButton *studyDatumBtn;
@property (weak, nonatomic) IBOutlet UIButton *searchBtn;

@property (nonatomic,strong)UIView * underLineView;
@property (nonatomic,strong)UIButton * currentPageBtn;

@end

@implementation LearnHeaderView

#pragma mark - UI
-(UIView *)underLineView{
    if(!_underLineView){
        _underLineView=[[UIView alloc]init];
        UIView *view = [[UIView alloc] init];
        view.frame = CGRectMake(0,0,UnderLineViewW,4);
        CAGradientLayer *gradientLayer = [CAGradientLayer layer];
        gradientLayer.frame = view.bounds;
        gradientLayer.colors = @[(__bridge id)[UIColor colorWithRed:254/255.0 green:202/255.0 blue:64/255.0 alpha:1.0].CGColor, (__bridge id)[UIColor colorWithRed:255/255.0 green:147/255.0 blue:62/255.0 alpha:1.0].CGColor];
        gradientLayer.locations =  @[@(0), @(1.0f)];
        gradientLayer.startPoint = CGPointMake(0, 0.5);
        gradientLayer.endPoint = CGPointMake(1, 0.5);
        //  添加渐变色到创建的 UIView 上去
        [self.underLineView.layer addSublayer:gradientLayer];
    }
    return _underLineView;
}

-(void)awakeFromNib{
    [super awakeFromNib];
    self.currentPageBtn = self.studentBtn;
    [self addSubview:self.underLineView];
    [self.underLineView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.bottom.equalTo(self.studentBtn).offset(-6);
        make.width.mas_equalTo(self.currentPageBtn.frame.size.width);
        make.height.mas_equalTo(UnderLineViewH);
        make.centerX.equalTo(self.studentBtn);
    }];
}

//三个按钮tag分别是120，121，122, 123
- (IBAction)pageBtnClick:(UIButton *)sender {
    if(self.currentPageBtn == sender)return;
    if ([self.delegate respondsToSelector:@selector(pageControlBtnClickWithIndex:)]) {
        [self.delegate pageControlBtnClickWithIndex:sender.tag];
    }
    
    [self changePageWithBtn:sender];
    NSNotification *notification =[NSNotification notificationWithName:@"changePageIndex" object:[NSNumber numberWithInteger:sender.tag-BtnTag] userInfo:nil];
    [[NSNotificationCenter defaultCenter] postNotification:notification];
}

-(void)changePageWithIndex:(NSInteger)index{
    if(self.currentPageBtn.tag - BtnTag == index)return;
    UIButton *btn = [self viewWithTag:BtnTag+index];
    [self changePageWithBtn:btn];
}

-(void)changePageWithBtn:(UIButton *)sender{
    self.currentPageBtn.titleLabel.font = LLFFontPingFangSC(16);
    self.currentPageBtn.titleLabel.font = [UIFont systemFontOfSize:16 weight:1];
    self.currentPageBtn.selected=NO;
    self.currentPageBtn=sender;
    self.currentPageBtn.selected=YES;
    self.currentPageBtn.titleLabel.font = LLFFontPingFangSC(18);
    self.currentPageBtn.titleLabel.font = [UIFont systemFontOfSize:18 weight:1];
    [UIView animateWithDuration:0.25 animations:^{
        [self.underLineView mas_remakeConstraints:^(MASConstraintMaker *make) {
            make.centerX.equalTo(sender);
            make.bottom.equalTo(sender).offset(-6);
            make.width.mas_equalTo(sender.frame.size.width);
            make.height.mas_equalTo(UnderLineViewH);
        }];
        [self layoutIfNeeded];
    }];
}

@end
