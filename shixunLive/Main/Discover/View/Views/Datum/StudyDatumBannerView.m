//
//  StudyDatumBannerView.m
//  shixunLive
//
//  Created by apple on 2020/1/7.
//  Copyright © 2020 MSEducation. All rights reserved.
//

#import "StudyDatumBannerView.h"

#define BannerW (MYScreenWidth-88-24)
#define BannerH (MYScreenWidth-88-24)/16*9
@interface StudyDatumBannerView ()

@property(nonatomic,strong)UIImageView * bannerImg;

@end

@implementation StudyDatumBannerView

-(instancetype)init{
    if(self = [super init]){
        [self layoutUI];
    }
    return self;
}

-(UIImageView *)bannerImg{
    if(!_bannerImg){
        _bannerImg=[[UIImageView alloc]init];
        _bannerImg.layer.cornerRadius=8;
        _bannerImg.layer.masksToBounds=YES;
    }
    return _bannerImg;
}

-(void)layoutUI{
    [self addSubview:self.bannerImg];
    [self.bannerImg mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.left.offset(12);
        make.height.mas_equalTo(BannerH);
        make.right.bottom.equalTo(self).offset(-12);
    }];
}

@end
