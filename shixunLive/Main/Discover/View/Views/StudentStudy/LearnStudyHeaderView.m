//
//  LearnStudyHeaderView.m
//  shixunLive
//
//  Created by apple on 2020/1/4.
//  Copyright © 2020 MSEducation. All rights reserved.
//

#import "LearnStudyHeaderView.h"
#import "LearnStudyReportView.h"
#import "LearnStudySectionHeaderView.h"
#import <BHInfiniteScrollView/BHInfiniteScrollView.h>

#define BannerW MYScreenWidth-32
#define BannerH MYScreenHeight*0.3
@interface LearnStudyHeaderView ()<BHInfiniteScrollViewDelegate,LearnStudySectionHeaderViewDelegate>
///学习报告
@property (nonatomic,strong)LearnStudyReportView * reportView;
///轮播图
@property (nonatomic,strong)BHInfiniteScrollView* infinitePageView;

@property (nonatomic,strong)LearnStudySectionHeaderView * sectionHeaderView;

@end

@implementation LearnStudyHeaderView
-(instancetype)init{
    if(self = [super init]){
        [self layoutUI];
    }
    return self;
}
#pragma mark - 初始化控件
-(BHInfiniteScrollView *)infinitePageView{
    if(!_infinitePageView){
        _infinitePageView = [BHInfiniteScrollView infiniteScrollViewWithFrame:CGRectMake(0, 0,BannerW,BannerH) Delegate:self ImagesArray:nil];
        _infinitePageView.dotSize=4.0f;
        _infinitePageView.dotSpacing=5.0f;
        _infinitePageView.layer.cornerRadius=8;
        _infinitePageView.layer.masksToBounds=YES;
        _infinitePageView.dotColor=LLFColor(51, 51, 51);
        _infinitePageView.selectedDotColor=[UIColor whiteColor];
        _infinitePageView.placeholderImage=[UIImage imageNamed:@"picture_white"];
    }
    return _infinitePageView;
}

-(LearnStudyReportView *)reportView{
    if(!_reportView){
        _reportView=[[NSBundle mainBundle]loadNibNamed:@"LearnStudyReportView" owner:self options:nil].firstObject;
    }
    return _reportView;
}

-(LearnStudySectionHeaderView *)sectionHeaderView{
    if(!_sectionHeaderView){
        _sectionHeaderView=[[NSBundle mainBundle]loadNibNamed:@"LearnStudySectionHeaderView" owner:self options:nil].firstObject;
        _sectionHeaderView.delegate=self;
    }
    return _sectionHeaderView;
}

#pragma mark - LearnStudySectionHeaderViewDelegate
-(void)sectionHeaderViewFunctionBtnClickWithPageType:(LearnStudySectionHeaderType)sectionType{
    if ([self.delegate respondsToSelector:@selector(learnStudyViewCheckDetailReport)]) {
        [self.delegate learnStudyViewCheckDetailReport];
    }
}

#pragma mark - 布局
-(void)layoutUI{
    [self addSubview:self.infinitePageView];
    [self addSubview:self.sectionHeaderView];
    [self addSubview:self.reportView];
    [self.infinitePageView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self);
        make.centerX.equalTo(self);
        make.width.mas_equalTo(BannerW);
        make.height.mas_equalTo(BannerH);
    }];
    
    [self.sectionHeaderView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.infinitePageView.mas_bottom).offset(20);
        make.left.equalTo(self);
        make.height.mas_equalTo(40);
        make.width.mas_equalTo(MYScreenWidth);
    }];
    
    [self.reportView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.sectionHeaderView.mas_bottom).offset(12);
        make.centerX.equalTo(self.infinitePageView);
        make.width.mas_equalTo(BannerW);
        make.height.mas_equalTo(93);
        make.bottom.equalTo(self).offset(-12);
    }];
}

@end
