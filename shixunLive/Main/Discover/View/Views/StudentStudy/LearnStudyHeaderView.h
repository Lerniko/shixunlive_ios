//
//  LearnStudyHeaderView.h
//  shixunLive
//
//  Created by apple on 2020/1/4.
//  Copyright © 2020 MSEducation. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@protocol LearnStudyHeaderViewDelegate <NSObject>

-(void)learnStudyViewCheckDetailReport;

@end

@interface LearnStudyHeaderView : UIView

@property (nonatomic,weak)id<LearnStudyHeaderViewDelegate> delegate;

@end

NS_ASSUME_NONNULL_END
