//
//  LearnStudySectionHeaderView.m
//  shixunLive
//
//  Created by apple on 2020/1/4.
//  Copyright © 2020 MSEducation. All rights reserved.
//

#import "LearnStudySectionHeaderView.h"

@interface LearnStudySectionHeaderView ()

@property (weak, nonatomic) IBOutlet UIView *yellowView;

@property (weak, nonatomic) IBOutlet UILabel *titleLabel;

@property (weak, nonatomic) IBOutlet UILabel *subTitleLabel;

@property (weak, nonatomic) IBOutlet UIButton *functionBtn;

@end

@implementation LearnStudySectionHeaderView

-(void)awakeFromNib{
    [super awakeFromNib];
    UIView *view = [[UIView alloc] init];
    view.frame = CGRectMake(16,338,4,14);
        CAGradientLayer *gl = [CAGradientLayer layer];
        gl.frame = CGRectMake(16,338,4,14);
        gl.startPoint = CGPointMake(0, 0.5);
        gl.endPoint = CGPointMake(1, 0.5);
        gl.colors = @[(__bridge id)[UIColor colorWithRed:254/255.0 green:202/255.0 blue:64/255.0 alpha:1.0].CGColor, (__bridge id)[UIColor colorWithRed:255/255.0 green:147/255.0 blue:62/255.0 alpha:1.0].CGColor];
        gl.locations = @[@(0), @(1.0f)];
    view.layer.cornerRadius = 2;
    [self.yellowView.layer addSublayer:gl];
}

-(void)refreshUI{
    switch (_sectionType) {
        case 0:
        {
            self.titleLabel.text = @"每日一听";
        }
            break;
        case 1:
        {
            self.titleLabel.text = @"推荐";
            self.subTitleLabel.text = @"/必修课";
            self.subTitleLabel.hidden = NO;
        }
            break;
        case 2:
        {
            self.titleLabel.text = @"推荐";
            self.subTitleLabel.text = @"/资源";
            self.subTitleLabel.hidden = NO;
        }
            break;
            
        default:
            break;
    }
}

-(void)setSectionType:(LearnStudySectionHeaderType)sectionType{
    _sectionType = sectionType;
    [self refreshUI];
}

- (IBAction)funBtnClick:(id)sender {
    if ([self.delegate respondsToSelector:@selector(sectionHeaderViewFunctionBtnClickWithPageType:)]) {
        [self.delegate sectionHeaderViewFunctionBtnClickWithPageType:_sectionType];
    }
}

@end
