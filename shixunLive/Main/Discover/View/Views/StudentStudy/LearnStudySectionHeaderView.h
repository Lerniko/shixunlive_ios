//
//  LearnStudySectionHeaderView.h
//  shixunLive
//
//  Created by apple on 2020/1/4.
//  Copyright © 2020 MSEducation. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN
typedef enum : NSUInteger {
    LearnStudySectionHeaderTypeListen = 0,        //每日一听
    LearnStudySectionHeaderTypePoint = 1,        //必修
    LearnStudySectionHeaderTypeData     //资料
} LearnStudySectionHeaderType;

@protocol LearnStudySectionHeaderViewDelegate <NSObject>

-(void)sectionHeaderViewFunctionBtnClickWithPageType:(LearnStudySectionHeaderType)sectionType;

@end

@interface LearnStudySectionHeaderView : UIView

@property (nonatomic,assign)LearnStudySectionHeaderType sectionType;

@property(nonatomic,weak)id<LearnStudySectionHeaderViewDelegate> delegate;

@end

NS_ASSUME_NONNULL_END
