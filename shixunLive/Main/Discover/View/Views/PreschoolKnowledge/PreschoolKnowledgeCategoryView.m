//
//  PreschoolKnowledgeCategoryView.m
//  shixunLive
//
//  Created by apple on 2020/1/7.
//  Copyright © 2020 MSEducation. All rights reserved.
//

#import "PreschoolKnowledgeCategoryView.h"

#define CategoryViewW 88
#define SingleBtnH 45
@interface PreschoolKnowledgeCategoryView ()

@property (nonatomic,strong)PreschoolKnowledgeSingleCategoryView * lastView;

@end

@implementation PreschoolKnowledgeCategoryView

-(void)setListArray:(NSArray *)listArray{
    _listArray = listArray;
    for(int i = 0;i<self.listArray.count;i++){
        PreschoolKnowledgeSingleCategoryView * view = [[PreschoolKnowledgeSingleCategoryView alloc]initWithFirstCategoryTitle:self.listArray[i]];
        view.layer.cornerRadius=8;
        view.layer.masksToBounds=YES;
        [self addSubview:view];
        if(i==0){
            [view mas_makeConstraints:^(MASConstraintMaker *make) {
                make.top.equalTo(self);
                make.left.right.equalTo(self);
                make.width.mas_equalTo(CategoryViewW);
                make.height.mas_equalTo(SingleBtnH);
            }];
        }else if(i==self.listArray.count-1){
            [view mas_makeConstraints:^(MASConstraintMaker *make) {
                make.top.equalTo(self.lastView.mas_bottom).offset(10);
                make.left.right.equalTo(self);
                make.bottom.equalTo(self);
                make.width.mas_equalTo(CategoryViewW);
                make.height.mas_equalTo(SingleBtnH);
            }];
        }else{
            [view mas_makeConstraints:^(MASConstraintMaker *make) {
                make.top.equalTo(self.lastView.mas_bottom).offset(10);
                make.left.right.equalTo(self);
                make.width.mas_equalTo(CategoryViewW);
                make.height.mas_equalTo(SingleBtnH);
            }];
        }
        self.lastView = view;
        [view refreshCategoryViewHeight];
    }
}

@end


#define BtnH 45
#define BtnW 88

@interface PreschoolKnowledgeSingleCategoryView ()

@property(nonatomic,strong)UILabel * headerLabel;

@property(nonatomic,strong)UIView * yellowLineView;

@property(nonatomic,strong)NSString * firstCategoryId;

@property(nonatomic,strong)NSMutableArray * listArray;

@end

@implementation PreschoolKnowledgeSingleCategoryView

-(instancetype)initWithFirstCategoryTitle:(NSString *)title{
    if(self = [super init]){
        self.firstCategoryId = title;
        [self layoutUI];
    }
    return self;
}

#pragma mark - 数据
-(NSMutableArray *)listArray{
    if(!_listArray){
        _listArray=[NSMutableArray new];
    }
    return _listArray;
}

#pragma mark - UI
-(UILabel *)headerLabel{
    if(!_headerLabel){
        _headerLabel=[[UILabel alloc]init];
        _headerLabel.font = LLFFontPingFangSC(14);
        _headerLabel.font = LLFFontWeight(14, 1);
        _headerLabel.textColor = LLFColor(107, 107, 107);
        _headerLabel.textAlignment=NSTextAlignmentCenter;
        _headerLabel.backgroundColor = LLFColor(255, 206, 181);
    }
    return _headerLabel;
}

-(UIView *)yellowLineView{
    if(!_yellowLineView){
        _yellowLineView=[[UIView alloc]init];
        _yellowLineView.backgroundColor = LLFColor(255, 178, 0);
        _yellowLineView.layer.cornerRadius = 2;
    }
    return _yellowLineView;
}

-(void)layoutUI{
    self.headerLabel.text = self.firstCategoryId;
    [self addSubview:self.headerLabel];
    [self.headerLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.top.equalTo(self);
        make.height.mas_equalTo(BtnH);
        make.width.mas_equalTo(BtnW);
    }];
}

-(void)refreshCategoryViewHeight{
    self.listArray=[NSMutableArray arrayWithArray:@[@"家园共育",@"一日流程",@"幼儿行为",@"环境创设",@"其他"]];
    [self mas_updateConstraints:^(MASConstraintMaker *make) {
        make.height.mas_equalTo(self.listArray.count*SingleBtnH+SingleBtnH);
    }];
    UIButton * lastBtn = nil;
    for(int i = 0;i<self.listArray.count;i++){
        UIButton * btn = [UIButton buttonWithType:0];
        [btn setTitle:self.listArray[i] forState:0];
        [btn setTitleColor:LLFColor(102, 102, 102) forState:0];
        [btn setTitleColor:LLFColor(51, 51, 51) forState:UIControlStateSelected];
        btn.titleLabel.font = LLFFontPingFangSC(14);
        btn.backgroundColor = LLFColor(250, 250, 250);
        [self addSubview:btn];
        //如果只有一个
        if(self.listArray.count==1){
            [btn mas_makeConstraints:^(MASConstraintMaker *make) {
                make.top.equalTo(self.headerLabel.mas_bottom);
                make.left.equalTo(self);
                make.width.mas_equalTo(BtnW);
                make.height.mas_equalTo(BtnH);
                make.bottom.equalTo(self);
            }];
        }else{
            if(i==0){//第一个
                [btn mas_makeConstraints:^(MASConstraintMaker *make) {
                    make.top.equalTo(self.headerLabel.mas_bottom);
                    make.left.equalTo(self);
                    make.width.mas_equalTo(BtnW);
                    make.height.mas_equalTo(BtnH);
                }];
            }else if(i==self.listArray.count-1){//最后一个
                [btn mas_makeConstraints:^(MASConstraintMaker *make) {
                    make.top.equalTo(lastBtn.mas_bottom);
                    make.left.equalTo(self);
                    make.width.mas_equalTo(BtnW);
                    make.height.mas_equalTo(BtnH);
                    make.bottom.equalTo(self);
                }];
            }else{
                [btn mas_makeConstraints:^(MASConstraintMaker *make) {
                    make.top.equalTo(lastBtn.mas_bottom);
                    make.left.equalTo(self);
                    make.width.mas_equalTo(BtnW);
                    make.height.mas_equalTo(BtnH);
                }];
            }
            lastBtn = btn;
        }
    }
}

@end
