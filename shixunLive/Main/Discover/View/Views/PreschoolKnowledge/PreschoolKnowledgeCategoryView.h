//
//  PreschoolKnowledgeCategoryView.h
//  shixunLive
//
//  Created by apple on 2020/1/7.
//  Copyright © 2020 MSEducation. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface PreschoolKnowledgeSingleCategoryView : UIView

-(instancetype)initWithFirstCategoryTitle:(NSString *)title;

-(void)refreshCategoryViewHeight;

@end

@interface PreschoolKnowledgeCategoryView : UIScrollView
///一级分类
@property(nonatomic,strong)NSArray * listArray;

@end

