//
//  RadioCategoryView.m
//  shixunLive
//
//  Created by apple on 2020/1/7.
//  Copyright © 2020 MSEducation. All rights reserved.
//

#import "RadioCategoryView.h"

#define BtnW (MYScreenWidth-5*9)/4
#define BtnH 28
@implementation RadioCategoryView

-(void)setListArray:(NSArray *)listArray{
    for (int i = 0; i<listArray.count; i++) {
        int colum = i%4;
        int row = i/4;
        UIButton * btn = [UIButton buttonWithType:0];
        btn.frame = CGRectMake(colum*(9+BtnW),row*(8+BtnH), BtnW, BtnH);
        [btn setTitle:listArray[i] forState:0];
        [btn setTitleColor:LLFColor(51, 51, 51) forState:0];
        btn.backgroundColor = LLFColor(250, 250, 250);
        btn.titleLabel.font=LLFFontPingFangSC(12);
        btn.titleLabel.font=LLFFontWeight(12, 1);
        btn.layer.cornerRadius=4;
        btn.layer.masksToBounds=YES;
        [self addSubview:btn];
        if(i==listArray.count-1){
            
        }
    }
}

@end
