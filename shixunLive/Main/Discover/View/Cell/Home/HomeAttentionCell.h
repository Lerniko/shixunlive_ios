//
//  HomeAttentionCell.h
//  shixunLive
//
//  Created by apple on 2019/12/31.
//  Copyright © 2019 MSEducation. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN
@class ArticleListModel;
@interface HomeAttentionCell : UICollectionViewCell

@property (nonatomic,strong)ArticleListModel * model;

@end

NS_ASSUME_NONNULL_END
