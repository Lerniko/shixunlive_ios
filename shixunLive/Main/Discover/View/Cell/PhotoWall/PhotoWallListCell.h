//
//  PhotoWallListCell.h
//  shixunLive
//
//  Created by apple on 2020/1/2.
//  Copyright © 2020 MSEducation. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN
@class PhotoWallListModel;
@interface PhotoWallListCell : UICollectionViewCell

@property (nonatomic,strong)PhotoWallListModel * model;

@end

NS_ASSUME_NONNULL_END
