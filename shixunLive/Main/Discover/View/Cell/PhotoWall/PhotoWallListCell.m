//
//  PhotoWallListCell.m
//  shixunLive
//
//  Created by apple on 2020/1/2.
//  Copyright © 2020 MSEducation. All rights reserved.
//

#import "PhotoWallListCell.h"
#import "PhotoWallModel.h"

@interface PhotoWallListCell ()

@property (weak, nonatomic) IBOutlet UIImageView *coverImg;

@property (weak, nonatomic) IBOutlet UILabel *titleLabel;

@property (weak, nonatomic) IBOutlet UIImageView *userIcon;

@property (weak, nonatomic) IBOutlet UILabel *userNameLabel;

@property (weak, nonatomic) IBOutlet UILabel *praiseCountLabel;

@end

@implementation PhotoWallListCell

-(void)setModel:(PhotoWallListModel *)model{
    self.titleLabel.text = model.title;
    NSMutableDictionary * dic = [NSMutableDictionary dictionary];
    dic[NSFontAttributeName] = [UIFont systemFontOfSize:13];
    CGRect textRect = [model.title boundingRectWithSize:CGSizeMake((MYScreenWidth-50), MAXFLOAT)  options:NSStringDrawingUsesLineFragmentOrigin attributes:dic context:nil];
    model.h=[NSString stringWithFormat:@"%.2f",textRect.size.height];
    [self.userIcon sd_setImageWithURL:[NSURL URLWithString:model.userImg] placeholderImage:[UIImage imageNamed:@"vip"]];
    self.userNameLabel.text = [[CommonCodeManager shareManager]digitalEmojiToUnicode:model.userName];
    self.praiseCountLabel.text = model.pv;
    [self.coverImg sd_setImageWithURL:[NSURL URLWithString:model.imgList[0]] completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, NSURL *imageURL) {
        model.w = [NSString stringWithFormat:@"%.2f",image.size.width];
        model.h = [NSString stringWithFormat:@"%.2f",[model.h floatValue]+image.size.height];
    }];
}

-(void)drawRect:(CGRect)rect{
    //  创建 CAGradientLayer 对象
    CAGradientLayer *gradientLayer = [CAGradientLayer layer];
    //  设置 gradientLayer 的 Frame
    gradientLayer.frame = self.coverImg.bounds;
    //  创建渐变色数组，需要转换为CGColor颜色
    gradientLayer.colors = @[(id)[UIColor clearColor].CGColor,
                             (id)[UIColor blackColor].CGColor];
    //  设置三种颜色变化点，取值范围 0.0~1.0
    gradientLayer.locations = @[@(0.7f)];
    //  设置渐变颜色方向，左上点为(0,0), 右下点为(1,1)
    gradientLayer.startPoint = CGPointMake(1,0);
    gradientLayer.endPoint = CGPointMake(1, 1);
    //  添加渐变色到创建的 UIView 上去
    [self.coverImg.layer addSublayer:gradientLayer];
}

@end
