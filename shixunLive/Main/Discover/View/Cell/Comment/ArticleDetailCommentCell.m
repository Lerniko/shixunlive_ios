//
//  ArticleDetailCommentCell.m
//  shixunLive
//
//  Created by apple on 2020/1/6.
//  Copyright © 2020 MSEducation. All rights reserved.
//

#import "ArticleDetailCommentCell.h"
#import "ArticleCommentReplyGrayView.h"

#define IconSize 32
@interface ArticleDetailCommentCell ()

@property (nonatomic,strong)UIImageView * iconView;
@property (nonatomic,strong)UILabel * userNameLabel;
@property (nonatomic,strong)UILabel * createTimeLabel;
@property (nonatomic,strong)UILabel * contentLabel;
@property (nonatomic,strong)ArticleCommentReplyGrayView * replyView;

@end

@implementation ArticleDetailCommentCell

-(instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier{
    if(self = [super initWithStyle:style reuseIdentifier:reuseIdentifier]){
        [self layoutUI];
    }
    return self;
}

#pragma mark - UI
-(UIImageView *)iconView{
    if(!_iconView){
        _iconView=[[UIImageView alloc]init];
        _iconView.layer.cornerRadius=IconSize/2;
        _iconView.layer.masksToBounds=YES;
    }
    return _iconView;
}

-(UILabel *)userNameLabel{
    if(!_userNameLabel){
        _userNameLabel=[[UILabel alloc]init];
        _userNameLabel.font = LLFFontPingFangSC(14);
        _userNameLabel.font = LLFFontWeight(14, 1);
        _userNameLabel.textColor=LLFColor(51, 51, 51);
        _userNameLabel.text = @"用户名";
    }
    return _userNameLabel;
}

-(UILabel *)createTimeLabel{
    if(!_createTimeLabel){
        _createTimeLabel=[[UILabel alloc]init];
        _createTimeLabel.font = LLFFontPingFangSC(12);
        _createTimeLabel.textColor=LLFColor(153,153,153);
        _createTimeLabel.text = @"0分钟前";
    }
    return _createTimeLabel;
}

-(UILabel *)contentLabel{
    if(!_contentLabel){
        _contentLabel=[[UILabel alloc]init];
        _contentLabel.font = LLFFontPingFangSC(14);
        _contentLabel.textColor=LLFColor(51,51,51);
        _contentLabel.numberOfLines=2;
        _contentLabel.text = @"内容";
    }
    return _contentLabel;
}

-(ArticleCommentReplyGrayView *)replyView{
    if(!_replyView){
        _replyView=[[ArticleCommentReplyGrayView alloc]init];
        _replyView.clipsToBounds=YES;
    }
    return _replyView;
}

#pragma mark - 布局
-(void)layoutUI{
    [self addSubview:self.iconView];
    [self addSubview:self.userNameLabel];
    [self addSubview:self.createTimeLabel];
    [self addSubview:self.contentLabel];
    [self addSubview:self.replyView];
    [self.iconView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.top.equalTo(self).offset(DefaultMargin);
        make.height.width.mas_equalTo(IconSize);
    }];
    
    [self.userNameLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.iconView);
        make.left.equalTo(self.iconView.mas_right).offset(8);
        make.height.mas_equalTo(14);
    }];
    
    [self.createTimeLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.userNameLabel.mas_bottom).offset(8);
        make.left.equalTo(self.userNameLabel);
        make.height.mas_equalTo(12);
    }];
    
    [self.contentLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.createTimeLabel.mas_bottom).offset(10);
        make.left.equalTo(self.createTimeLabel);
        make.right.equalTo(self).offset(-DefaultMargin);
    }];
    
    [self.replyView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.contentLabel.mas_bottom).offset(5);
        make.left.equalTo(self.contentLabel);
        make.right.equalTo(self).offset(-16);
        make.height.mas_equalTo(80);
        make.bottom.equalTo(self).offset(-DefaultMargin);
    }];
}

-(void)drawRect:(CGRect)rect{
    CGContextRef context = UIGraphicsGetCurrentContext();
    CGContextBeginPath(context);
    CGContextMoveToPoint(context,self.replyView.frame.origin.x+20,self.replyView.frame.origin.y-10);//设置起始位置
    CGContextAddLineToPoint(context,self.replyView.frame.origin.x+14,self.replyView.frame.origin.y);//从起始位置到这个点连线
    CGContextAddLineToPoint(context,self.replyView.frame.origin.x+26,self.replyView.frame.origin.y);
    CGContextClosePath(context);//结束画线..自动封闭  不写也可封闭
    [LLFColor(238, 238, 238) setFill]; //设置填充色 不设置默认黑色
    [[UIColor clearColor] setStroke];//边框颜色，不设置默认黑色
    CGContextDrawPath(context, kCGPathFillStroke);//绘制路径
}

@end
