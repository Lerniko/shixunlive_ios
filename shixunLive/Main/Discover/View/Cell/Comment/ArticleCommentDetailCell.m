//
//  ArticleCommentDetailCell.m
//  shixunLive
//
//  Created by apple on 2020/1/6.
//  Copyright © 2020 MSEducation. All rights reserved.
//

#import "ArticleCommentDetailCell.h"

#define IconSize 32
@interface ArticleCommentDetailCell ()

@property (nonatomic,strong)UIImageView * iconView;
@property (nonatomic,strong)UILabel * userNameLabel;
@property (nonatomic,strong)UILabel * createTimeLabel;
@property (nonatomic,strong)UILabel * contentLabel;
@property (nonatomic,strong)UIButton * checkSourceBtn;

@end

@implementation ArticleCommentDetailCell

-(instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier{
    if(self = [super initWithStyle:style reuseIdentifier:reuseIdentifier]){
        [self layoutUI];
    }
    return self;
}

#pragma mark - UI
-(UIImageView *)iconView{
    if(!_iconView){
        _iconView=[[UIImageView alloc]init];
        _iconView.layer.cornerRadius=IconSize/2;
        _iconView.layer.masksToBounds=YES;
    }
    return _iconView;
}

-(UILabel *)userNameLabel{
    if(!_userNameLabel){
        _userNameLabel=[[UILabel alloc]init];
        _userNameLabel.font = LLFFontPingFangSC(14);
        _userNameLabel.font = LLFFontWeight(14, 1);
        _userNameLabel.textColor=LLFColor(51, 51, 51);
        _userNameLabel.text = @"用户名";
    }
    return _userNameLabel;
}

-(UILabel *)createTimeLabel{
    if(!_createTimeLabel){
        _createTimeLabel=[[UILabel alloc]init];
        _createTimeLabel.font = LLFFontPingFangSC(12);
        _createTimeLabel.textColor=LLFColor(153,153,153);
        _createTimeLabel.text = @"0分钟前";
    }
    return _createTimeLabel;
}

-(UILabel *)contentLabel{
    if(!_contentLabel){
        _contentLabel=[[UILabel alloc]init];
        _contentLabel.font = LLFFontPingFangSC(14);
        _contentLabel.textColor=LLFColor(51,51,51);
        _contentLabel.numberOfLines=2;
        _contentLabel.text = @"内容";
    }
    return _contentLabel;
}

-(UIButton *)checkSourceBtn{
    if(!_checkSourceBtn){
        _checkSourceBtn=[UIButton buttonWithType:0];
        _checkSourceBtn.titleLabel.font = LLFFontPingFangSC(14);
        _checkSourceBtn.titleLabel.font = LLFFontWeight(14, 1);
        [_checkSourceBtn setTitle:@"查看原话题" forState:0];
        [_checkSourceBtn setTitleColor:LLFColor(34, 34, 34) forState:0];
    }
    return _checkSourceBtn;
}

#pragma mark - 布局
-(void)layoutUI{
    [self addSubview:self.iconView];
    [self addSubview:self.userNameLabel];
    [self addSubview:self.createTimeLabel];
    [self addSubview:self.contentLabel];
    [self addSubview:self.checkSourceBtn];
    [self.iconView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.top.equalTo(self).offset(DefaultMargin);
        make.height.width.mas_equalTo(IconSize);
    }];
    
    [self.userNameLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.iconView);
        make.left.equalTo(self.iconView.mas_right).offset(8);
        make.height.mas_equalTo(14);
    }];
    
    [self.createTimeLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.userNameLabel.mas_bottom).offset(8);
        make.left.equalTo(self.userNameLabel);
        make.height.mas_equalTo(12);
    }];
    
    [self.contentLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.createTimeLabel.mas_bottom).offset(10);
        make.left.equalTo(self.createTimeLabel);
        make.right.equalTo(self).offset(-DefaultMargin);
        make.bottom.equalTo(self).offset(-DefaultMargin);
    }];
    
//    [self.checkSourceBtn mas_makeConstraints:^(MASConstraintMaker *make) {
//        make.top.equalTo(self.contentLabel.mas_bottom).offset(DefaultMargin);
//        make.left.equalTo(self.contentLabel);
//        make.bottom.equalTo(self).offset(-DefaultMargin);
//    }];
}

@end
