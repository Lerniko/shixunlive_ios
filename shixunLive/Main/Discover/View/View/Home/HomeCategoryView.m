//
//  HomeCategoryView.m
//  shixunLive
//
//  Created by apple on 2019/12/31.
//  Copyright © 2019 MSEducation. All rights reserved.
//

#import "HomeCategoryView.h"

#define BtnW 64
#define BtnH 26

@interface HomeCategoryView ()

@property (nonatomic,strong)UIButton * currentBtn;

@end

@implementation HomeCategoryView

-(void)setCategoryArr:(NSArray *)categoryArr{
    for (int i = 0; i<categoryArr.count; i++) {
        UIButton * btn = [UIButton buttonWithType:0];
        btn.frame = CGRectMake(10+i*(BtnW+10),7.5, BtnW, BtnH);
        btn.layer.cornerRadius=4;
        btn.layer.masksToBounds=YES;
        btn.titleLabel.font = LLFFontPingFangSC(13);
        btn.titleLabel.font = LLFFontWeight(13, 1);
        [btn setTitleColor:LLFColor(153, 153, 153) forState:0];
        [btn setTitle:categoryArr[i] forState:0];
        [btn setTitleColor:[UIColor whiteColor] forState:UIControlStateSelected];
        if(i==0){
            [btn setBackgroundImage:[UIImage imageNamed:@"icon_btn_bgView"] forState:UIControlStateSelected];
            btn.selected=YES;
        }else{
            btn.layer.borderWidth=1;
            btn.layer.borderColor = LLFColor(240, 240, 240).CGColor;
            [btn setBackgroundImage:[UIImage imageNamed:@""] forState:0];
        }
        [btn addTarget:self action:@selector(categoryBtnClick:) forControlEvents:UIControlEventTouchUpInside];
        [self addSubview:btn];
    }
}

-(void)categoryBtnClick:(UIButton *)btn{
    
}

@end
