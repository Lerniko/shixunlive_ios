//
//  HomeCategoryView.h
//  shixunLive
//
//  Created by apple on 2019/12/31.
//  Copyright © 2019 MSEducation. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface HomeCategoryView : UIView

@property (nonatomic,strong)NSArray * categoryArr;

@end

NS_ASSUME_NONNULL_END
