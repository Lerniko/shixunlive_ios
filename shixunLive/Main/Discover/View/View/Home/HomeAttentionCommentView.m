//
//  HomeAttentionCommentView.m
//  shixunLive
//
//  Created by apple on 2019/12/31.
//  Copyright © 2019 MSEducation. All rights reserved.
//

#import "HomeAttentionCommentView.h"

#define CommentEditViewH 40
@interface HomeAttentionCommentView ()

@property(nonatomic,strong)HomeAttentionCommentEditView * commentEditView;

@end

@implementation HomeAttentionCommentView

-(instancetype)init{
    if (self = [super init]) {
        [self layoutUI];
    }
    return self;
}

#pragma mark - UI
-(HomeAttentionCommentEditView *)commentEditView{
    if(!_commentEditView){
        _commentEditView=[[HomeAttentionCommentEditView alloc]init];
    }
    return _commentEditView;
}

-(void)layoutUI{
    [self addSubview:self.commentEditView];
    [self.commentEditView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.right.bottom.equalTo(self);
        make.height.mas_equalTo(CommentEditViewH);
    }];
}

-(void)setCommentArr:(NSArray *)commentArr{
    _commentArr = commentArr;
    UILabel * lastLabel = nil;
    for(int i = 0;i<commentArr.count;i++){
        UILabel * label  = [[UILabel alloc]init];
        label.text = commentArr[i];
        label.font = LLFFont(13);
        label.numberOfLines=3;
        NSMutableDictionary * dic = [NSMutableDictionary dictionary];
        dic[NSFontAttributeName] = [UIFont systemFontOfSize:13];
        CGRect textRect = [label.text boundingRectWithSize:CGSizeMake((MYScreenWidth-20), MAXFLOAT)  options:NSStringDrawingUsesLineFragmentOrigin attributes:dic context:nil];
        [self addSubview:label];
        if(commentArr.count>1){
            if(i==0){
                [label mas_makeConstraints:^(MASConstraintMaker *make) {
                    make.left.equalTo(self).offset(10);
                    make.top.equalTo(self).offset(10);
                    make.right.equalTo(self).offset(-10);
                    make.height.mas_equalTo(textRect.size.height);
                }];
            }else if(i==commentArr.count-1){
                [label mas_makeConstraints:^(MASConstraintMaker *make) {
                    make.left.equalTo(self).offset(10);
                    make.top.equalTo(lastLabel.mas_bottom).offset(10);
                    make.right.equalTo(self).offset(-10);
                    make.height.mas_equalTo(textRect.size.height);
                    make.bottom.equalTo(self.commentEditView.mas_top);
                }];
            }else{
                [label mas_makeConstraints:^(MASConstraintMaker *make) {
                    make.left.equalTo(self).offset(10);
                    make.top.equalTo(lastLabel.mas_bottom).offset(10);
                    make.right.equalTo(self).offset(-10);
                    make.height.mas_equalTo(textRect.size.height);
                }];
            }
            lastLabel = label;
        }else{
            [label mas_makeConstraints:^(MASConstraintMaker *make) {
                make.left.equalTo(self).offset(10);
                make.top.equalTo(self).offset(10);
                make.right.equalTo(self).offset(-10);
                make.height.mas_equalTo(textRect.size.height);
                make.bottom.equalTo(self.commentEditView.mas_top);
            }];
        }
    }
}

@end

@interface HomeAttentionCommentEditView ()

@property (nonatomic,strong)UIImageView * penView;

@property (nonatomic,strong)UITextField * textField;

@end

@implementation HomeAttentionCommentEditView

-(instancetype)init{
    if(self = [super init]){
        [self layoutUI];
    }
    return self;
}

#pragma mark - UI
-(UIImageView *)penView{
    if(!_penView){
        _penView=[[UIImageView alloc]init];
        _penView.image = [UIImage imageNamed:@"icon_home_eidt"];
    }
    return _penView;
}

-(UITextField *)textField{
    if(!_textField){
        _textField=[[UITextField alloc]init];
        _textField.placeholder = @"说点什么";
        _textField.font = LLFFont(13);
    }
    return _textField;
}

-(void)layoutUI{
    [self addSubview:self.penView];
    [self addSubview:self.textField];
    [self.penView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self);
        make.centerY.equalTo(self);
        make.width.mas_equalTo(16);
        make.height.mas_equalTo(19);
    }];
    
    [self.textField mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self.penView.mas_right).offset(5);
        make.centerY.equalTo(self);
        make.right.equalTo(self).offset(-10);
    }];
}

@end
