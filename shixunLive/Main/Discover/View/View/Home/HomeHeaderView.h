//
//  HomeHeaderView.h
//  shixunLive
//
//  Created by apple on 2019/12/31.
//  Copyright © 2019 MSEducation. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@protocol HomeHeaderViewDelegate <NSObject>

-(void)pageControlBtnClickWithIndex:(NSInteger)index;

@end

@interface HomeHeaderView : UIView

-(void)changePageWithIndex:(NSInteger)index;

@property(nonatomic,weak)id<HomeHeaderViewDelegate> delegate;

@end

NS_ASSUME_NONNULL_END
