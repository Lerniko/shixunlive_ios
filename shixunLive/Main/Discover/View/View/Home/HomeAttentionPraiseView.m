//
//  HomeAttentionPraiseView.m
//  shixunLive
//
//  Created by apple on 2019/12/31.
//  Copyright © 2019 MSEducation. All rights reserved.
//

#import "HomeAttentionPraiseView.h"

#define UserIconTag 110
@interface HomeAttentionPraiseView ()

@property (nonatomic,strong)UIButton * praiseBtn;

@property (nonatomic,strong)UIButton * shareBtn;

@end

@implementation HomeAttentionPraiseView

-(instancetype)init{
    if(self = [super init]){
        [self layoutUI];
    }
    return self;
}

-(void)layoutUI{
    [self addSubview:self.praiseBtn];
    [self addSubview:self.shareBtn];
    [self.praiseBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self);
        make.centerY.equalTo(self);
        make.width.mas_equalTo(56);
        make.height.mas_equalTo(24);
    }];
    
    [self.shareBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.width.mas_equalTo(24);
        make.height.mas_equalTo(26);
        make.centerY.equalTo(self);
        make.right.equalTo(self);
    }];
}

#pragma mark - UI
-(UIButton *)praiseBtn{
    if(!_praiseBtn){
        _praiseBtn=[UIButton buttonWithType:0];
        [_praiseBtn setImage:[UIImage imageNamed:@"icon_home_praise_default"] forState:0];
        [_praiseBtn setImage:[UIImage imageNamed:@"icon_home_praise_selected"] forState:UIControlStateSelected];
        [_praiseBtn setTitle:@" 赞" forState:0];
        [_praiseBtn setTitleColor:LLFColor(153, 153, 153) forState:0];
        [_praiseBtn setTitleColor:[UIColor whiteColor] forState:UIControlStateSelected];
        _praiseBtn.backgroundColor = LLFColor(238, 238, 238);
        _praiseBtn.layer.cornerRadius=5;
        _praiseBtn.layer.masksToBounds=YES;
        _praiseBtn.titleLabel.font = LLFFontPingFangSC(13);
        [_praiseBtn setBackgroundImage:[UIImage imageNamed:@"icon_btn_bgView"] forState:UIControlStateSelected];
        [_praiseBtn addTarget:self action:@selector(praiseBtnClick:) forControlEvents:UIControlEventTouchUpInside];
    }
    return _praiseBtn;
}

-(UIButton *)shareBtn{
    if(!_shareBtn){
        _shareBtn=[UIButton buttonWithType:0];
        [_shareBtn setTitle:@"转发" forState:0];
        [_shareBtn setImage:[UIImage imageNamed:@"icon_home_zhuanfa"] forState:0];
        [_shareBtn addTarget:self action:@selector(shareBtnClick) forControlEvents:UIControlEventTouchUpInside];
    }
    return _shareBtn;
}

-(void)setPraiseArr:(NSArray *)praiseArr{
    for(int i = 0;i<praiseArr.count;i++){
        UIButton * userIconBtn = [UIButton buttonWithType:0];
        userIconBtn.layer.cornerRadius=2;
        userIconBtn.layer.masksToBounds=YES;
        userIconBtn.tag=UserIconTag+i;
        [userIconBtn setImage:[UIImage imageWithData:[NSData dataWithContentsOfURL:praiseArr[i]]] forState:0];
        [userIconBtn addTarget:self action:@selector(userIconClick:) forControlEvents:UIControlEventTouchUpInside];
    }
}

#pragma mark - 按钮事件
-(void)userIconClick:(UIButton *)btn{
    
}

-(void)shareBtnClick{
    
}

-(void)praiseBtnClick:(UIButton *)btn{
    btn.selected=!btn.selected;
}

@end
