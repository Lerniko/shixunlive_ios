//
//  ArticleCommentDetailCommentView.m
//  shixunLive
//
//  Created by apple on 2020/1/6.
//  Copyright © 2020 MSEducation. All rights reserved.
//

#import "ArticleCommentDetailCommentView.h"

@implementation ArticleCommentDetailCommentView

-(void)awakeFromNib{
    [super awakeFromNib];
    self.layer.shadowColor = [UIColor colorWithRed:238/255.0 green:238/255.0 blue:238/255.0 alpha:0.5].CGColor;
    self.layer.shadowOffset = CGSizeMake(0,-2);
    self.layer.shadowOpacity = 1;
    self.layer.shadowRadius = 10;
}

@end
