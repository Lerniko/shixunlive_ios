//
//  ArticleCommentSectionHeaderView.m
//  shixunLive
//
//  Created by apple on 2020/1/6.
//  Copyright © 2020 MSEducation. All rights reserved.
//

#import "ArticleCommentSectionHeaderView.h"

@interface ArticleCommentSectionHeaderView ()

@property (nonatomic,strong)UILabel * commentCountLabel;

@property (nonatomic,strong)UILabel * praiseCountLabel;

@end

@implementation ArticleCommentSectionHeaderView

-(instancetype)init{
    if(self = [super init]){
        [self layoutUI];
    }
    return self;
}

-(UILabel *)commentCountLabel{
    if(!_commentCountLabel){
        _commentCountLabel=[[UILabel alloc]init];
        _commentCountLabel.font = LLFFontPingFangSC(14);
        _commentCountLabel.font = LLFFontWeight(14, 1);
        _commentCountLabel.textColor=LLFColor(51, 51, 51);
        _commentCountLabel.text = @"评论 0";
    }
    return _commentCountLabel;
}

-(UILabel *)praiseCountLabel{
    if(!_praiseCountLabel){
        _praiseCountLabel=[[UILabel alloc]init];
        _praiseCountLabel.font = LLFFontPingFangSC(14);
        _praiseCountLabel.font = LLFFontWeight(14, 1);
        _praiseCountLabel.textColor=LLFColor(51, 51, 51);
        _praiseCountLabel.text = @"0人赞过";
    }
    return _praiseCountLabel;
}

-(void)layoutUI{
    [self addSubview:self.commentCountLabel];
    [self addSubview:self.praiseCountLabel];
    [self.commentCountLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self).offset(8);
        make.left.equalTo(self).offset(16);
    }];
    
    [self.praiseCountLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.equalTo(self).offset(-16);
        make.centerY.equalTo(self.commentCountLabel);
    }];
}

@end
