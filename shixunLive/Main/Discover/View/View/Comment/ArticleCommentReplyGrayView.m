//
//  ArticleCommentReplyGrayView.m
//  shixunLive
//
//  Created by apple on 2020/1/6.
//  Copyright © 2020 MSEducation. All rights reserved.
//

#import "ArticleCommentReplyGrayView.h"

@implementation ArticleCommentReplyGrayView

-(instancetype)init{
    if(self = [super init]){
        [self layoutUI];
    }
    return self;
}

-(void)layoutUI{
    self.backgroundColor=LLFColor(238, 238, 238);
    self.layer.cornerRadius = 4;
}

@end
