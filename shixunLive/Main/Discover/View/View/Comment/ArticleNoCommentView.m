//
//  ArticleNoCommentView.m
//  shixunLive
//
//  Created by apple on 2020/1/6.
//  Copyright © 2020 MSEducation. All rights reserved.
//

#import "ArticleNoCommentView.h"

@interface ArticleNoCommentView ()

@property (nonatomic,strong)UILabel * label;

@end

@implementation ArticleNoCommentView

-(instancetype)init{
    if(self = [super init]){
        [self layoutUI];
    }
    return self;
}

-(UILabel *)label{
    if(!_label){
        _label=[[UILabel alloc]init];
        _label.text = @"-暂无评论-";
        _label.font = LLFFontPingFangSC(14);
        _label.textColor = LLFColor(204, 204, 204);
    }
    return _label;
}

-(void)layoutUI{
    [self addSubview:self.label];
    [self.label mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerX.centerY.equalTo(self);
    }];
}

@end
