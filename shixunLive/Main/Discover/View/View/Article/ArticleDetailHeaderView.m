//
//  ArticleDetailHeaderView.m
//  shixunLive
//
//  Created by apple on 2020/1/6.
//  Copyright © 2020 MSEducation. All rights reserved.
//

#import "ArticleDetailHeaderView.h"

@interface ArticleDetailHeaderView ()

@property (weak, nonatomic) IBOutlet UIView *alphaView;


@end

@implementation ArticleDetailHeaderView

-(void)awakeFromNib{
    [super awakeFromNib];
    
    CAGradientLayer *gl = [CAGradientLayer layer];
    gl.frame = self.alphaView.bounds;
    gl.startPoint = CGPointMake(0.5, 0);
    gl.endPoint = CGPointMake(0.5, 1);
    gl.colors = @[(__bridge id)[UIColor colorWithRed:255/255.0 green:255/255.0 blue:255/255.0 alpha:0.0].CGColor, (__bridge id)[UIColor colorWithRed:255/255.0 green:255/255.0 blue:255/255.0 alpha:1.0].CGColor];
    gl.locations = @[@(0), @(1.0f)];
}

@end
