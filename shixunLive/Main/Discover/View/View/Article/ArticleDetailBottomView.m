//
//  ArticleDetailBottomView.m
//  shixunLive
//
//  Created by apple on 2020/1/6.
//  Copyright © 2020 MSEducation. All rights reserved.
//

#import "ArticleDetailBottomView.h"

@interface ArticleDetailBottomView()

@end

@implementation ArticleDetailBottomView

-(void)awakeFromNib{
    [super awakeFromNib];
    self.layer.shadowColor = [UIColor colorWithRed:238/255.0 green:238/255.0 blue:238/255.0 alpha:0.5].CGColor;
    self.layer.shadowOffset = CGSizeMake(0,-2);
    self.layer.shadowOpacity = 1;
    self.layer.shadowRadius = 10;
}

- (IBAction)praiseBtnClick:(UIButton *)sender {
    sender.selected = !sender.selected;
}

- (IBAction)collectBtnClick:(UIButton *)sender {
    sender.selected = !sender.selected;
}

@end
