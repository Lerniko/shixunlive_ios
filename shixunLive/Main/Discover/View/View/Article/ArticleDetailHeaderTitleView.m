//
//  ArticleDetailHeaderTitleView.m
//  shixunLive
//
//  Created by apple on 2020/1/6.
//  Copyright © 2020 MSEducation. All rights reserved.
//

#import "ArticleDetailHeaderTitleView.h"

@interface ArticleDetailHeaderTitleView()

@property (nonatomic,strong)UILabel * titleLabel;

@end

@implementation ArticleDetailHeaderTitleView

-(instancetype)initWithFrame:(CGRect)frame{
    if (self = [super initWithFrame:frame]) {
        [self layoutUI];
    }
    return self;
}

-(UILabel *)titleLabel{
    if(!_titleLabel){
        _titleLabel=[[UILabel alloc]init];
        _titleLabel.text = @"#标题#";
        _titleLabel.font = LLFFontPingFangSC(16);
        _titleLabel.font = LLFFontWeight(16, 1);
        _titleLabel.textColor=LLFColor(51, 51, 51);
    }
    return _titleLabel;
}

-(void)layoutUI{
    [self addSubview:self.titleLabel];
    [self.titleLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self).offset(16);
        make.left.equalTo(self).offset(16);
        make.right.equalTo(self).offset(-16);
        make.bottom.equalTo(self).offset(-16);
    }];
}

@end
