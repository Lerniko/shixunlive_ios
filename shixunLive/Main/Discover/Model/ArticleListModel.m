//
//  ArticleListModel.m
//  shixunLive
//
//  Created by apple on 2019/4/24.
//  Copyright © 2019 xsili. All rights reserved.
//

#import "ArticleListModel.h"

@implementation AdModel

@end

@implementation ArticleListModel

-(instancetype)initWithDictionary:(NSDictionary *)dict error:(NSError *__autoreleasing *)err{
    if(self =[super initWithDictionary:dict error:err]){
        if([self.isAd intValue]){
            _ad = [[AdModel alloc]initWithDictionary:dict[@"ad"] error:nil];
        }
    }
    return self;
}

@end
