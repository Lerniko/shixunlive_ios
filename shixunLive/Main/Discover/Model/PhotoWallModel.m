//
//  PhotoWallModel.m
//  shixunLive
//
//  Created by apple on 2019/9/23.
//  Copyright © 2019 xsili. All rights reserved.
//

#import "PhotoWallModel.h"

@implementation PhotoWallModel

@end

@implementation PhotoWallListModel

-(CGSize)scaleSize{
    CGFloat width = [UIScreen mainScreen].bounds.size.width;
    CGFloat scale = width / [self.w floatValue];
    CGFloat height = [self.h floatValue] * scale;
    
    return CGSizeMake(width, height);
}

@end

@implementation PhotoWallCommentModel

@end

