//
//  PhotoWallModel.h
//  shixunLive
//
//  Created by apple on 2019/9/23.
//  Copyright © 2019 xsili. All rights reserved.
//

#import "JSONModel.h"

NS_ASSUME_NONNULL_BEGIN

@interface PhotoWallModel : JSONModel

@property(nonatomic,strong)NSString<Optional> * title;

@property(nonatomic,strong)NSString<Optional> * userId;

@property(nonatomic,strong)NSString<Optional> * userImg;

@property(nonatomic,strong)NSString<Optional> * userName;

@property(nonatomic,strong)NSString<Optional> * auditUser;

@property(nonatomic,strong)NSString<Optional> * auditStatus;

@property(nonatomic,strong)NSString<Optional> * auditRemark;

@property(nonatomic,strong)NSArray<Optional> * categoryList;

@property(nonatomic,strong)NSString<Optional> * commentCount;

@property(nonatomic,strong)NSString<Optional> * content;

@property(nonatomic,strong)NSString<Optional> * createTime;

@property(nonatomic,strong)NSString<Optional> * pv;

@property(nonatomic,strong)NSArray<Optional> * commentList;

@property(nonatomic,strong)NSArray* imgList;

@end

@interface PhotoWallListModel : JSONModel

@property(nonatomic,strong)NSString<Optional> * id;

@property(nonatomic,strong)NSArray<Optional> * imgList;

@property(nonatomic,strong)NSString<Optional> * title;

@property(nonatomic,strong)NSString<Optional> * userImg;

@property(nonatomic,strong)NSString<Optional> * userName;

@property(nonatomic,strong)NSString<Optional> * pv;

@property (nonatomic,copy)NSString<Optional>* h;
@property (nonatomic,copy)NSString<Optional>* w;
-(CGSize)scaleSize;

@end

@interface PhotoWallCommentModel : JSONModel

@property(nonatomic,strong)NSString<Optional> * atlasId;

@property(nonatomic,strong)NSString<Optional> * commentId;

@property(nonatomic,strong)NSString<Optional> * content;

@property(nonatomic,strong)NSString<Optional> * floor;

@property(nonatomic,strong)NSString<Optional> * isFabulous;

@property(nonatomic,strong)NSString<Optional> * praise;

@property(nonatomic,strong)NSString<Optional> * title;

@property(nonatomic,strong)NSString<Optional> * userId;

@property(nonatomic,strong)NSString<Optional> * userImg;

@property(nonatomic,strong)NSString<Optional> * userName;

@property(nonatomic,strong)NSString<Optional> * createTime;

@property(nonatomic,strong)NSString<Optional> * cellHeight;

@end

NS_ASSUME_NONNULL_END
