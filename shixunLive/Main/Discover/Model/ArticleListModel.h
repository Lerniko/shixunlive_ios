//
//  ArticleListModel.h
//  shixunLive
//
//  Created by apple on 2019/4/24.
//  Copyright © 2019 xsili. All rights reserved.
//

#import "JSONModel.h"

NS_ASSUME_NONNULL_BEGIN

@interface AdModel : JSONModel

@property (nonatomic,strong)NSString<Optional> * id;
@property (nonatomic,strong)NSString<Optional> * url;
@property (nonatomic,strong)NSString<Optional> * title;
@property (nonatomic,strong)NSString<Optional> * img;
@property (nonatomic,strong)NSString<Optional> * describe;
@property (nonatomic,strong)NSString<Optional> * adPageType;
@property (nonatomic,strong)NSString<Optional> * typeValue;

@end

@interface ArticleListModel : JSONModel

@property (nonatomic,strong)NSString<Optional> * id;
@property (nonatomic,strong)NSString<Optional> * title;
@property (nonatomic,strong)NSString<Optional> * commentCount;
@property (nonatomic,strong)NSString<Optional> * praise;
@property (nonatomic,strong)NSString<Optional> * submitAuditTime;
@property (nonatomic,strong)NSString<Optional> * content;
@property (nonatomic,strong)NSString<Optional> * userImg;
@property (nonatomic,strong)NSString<Optional> * userName;
@property (nonatomic,strong)NSString<Optional> * userId;
@property (nonatomic,strong)NSArray<Optional> * coverImg;
@property (nonatomic,strong)NSArray<Optional> * accessoryList;
@property (nonatomic,strong)NSString<Optional> * isShow;
@property (nonatomic,strong)NSString<Optional> * pv;
@property (nonatomic,strong)NSString<Optional> * auditContent;
@property (nonatomic,strong)NSString<Optional> * categorySecondId;
@property (nonatomic,strong)NSString<Optional> * isRecommend;
@property (nonatomic,strong)NSString<Optional> * isFabulous;
@property (nonatomic,strong)NSString<Optional> * isAd;
@property (nonatomic,strong)NSString<Optional> * cellHeight;
@property (nonatomic,strong)AdModel<Optional> * ad;

@end

NS_ASSUME_NONNULL_END
